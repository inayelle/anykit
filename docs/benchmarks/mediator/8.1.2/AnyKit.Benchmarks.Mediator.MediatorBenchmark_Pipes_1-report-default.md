
BenchmarkDotNet v0.14.0, Arch Linux
AMD Ryzen 9 6900HX with Radeon Graphics, 1 CPU, 16 logical and 8 physical cores
.NET SDK 8.0.203
  [Host]     : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2
  DefaultJob : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2


 Method         | Mean     | Error   | StdDev  | Ratio | Gen0   | Allocated | Alloc Ratio |
--------------- |---------:|--------:|--------:|------:|-------:|----------:|------------:|
 Command_Class  | 350.9 ns | 1.36 ns | 1.27 ns |  1.00 | 0.0439 |     368 B |        1.00 |
 Command_Struct | 311.3 ns | 2.40 ns | 2.13 ns |  0.89 | 0.0439 |     368 B |        1.00 |
 Query_Class    | 400.6 ns | 2.96 ns | 2.62 ns |  1.14 | 0.0782 |     656 B |        1.78 |
 Query_Struct   | 297.6 ns | 1.73 ns | 1.62 ns |  0.85 | 0.0439 |     368 B |        1.00 |
