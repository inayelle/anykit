
BenchmarkDotNet v0.14.0, Arch Linux
AMD Ryzen 9 6900HX with Radeon Graphics, 1 CPU, 16 logical and 8 physical cores
.NET SDK 8.0.203
  [Host]     : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2
  DefaultJob : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2


 Method         | Mean     | Error   | StdDev  | Ratio | Gen0   | Allocated | Alloc Ratio |
--------------- |---------:|--------:|--------:|------:|-------:|----------:|------------:|
 Command_Class  | 339.3 ns | 1.64 ns | 1.54 ns |  1.00 | 0.0439 |     368 B |        1.00 |
 Command_Struct | 315.0 ns | 0.82 ns | 0.73 ns |  0.93 | 0.0439 |     368 B |        1.00 |
 Query_Class    | 392.7 ns | 4.04 ns | 3.78 ns |  1.16 | 0.0782 |     656 B |        1.78 |
 Query_Struct   | 333.1 ns | 1.96 ns | 1.83 ns |  0.98 | 0.0439 |     368 B |        1.00 |
