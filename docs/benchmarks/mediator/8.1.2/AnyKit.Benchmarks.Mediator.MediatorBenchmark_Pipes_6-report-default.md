
BenchmarkDotNet v0.14.0, Arch Linux
AMD Ryzen 9 6900HX with Radeon Graphics, 1 CPU, 16 logical and 8 physical cores
.NET SDK 8.0.203
  [Host]     : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2
  DefaultJob : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2


 Method         | Mean     | Error   | StdDev  | Ratio | Gen0   | Allocated | Alloc Ratio |
--------------- |---------:|--------:|--------:|------:|-------:|----------:|------------:|
 Command_Class  | 371.0 ns | 1.30 ns | 1.21 ns |  1.00 | 0.0439 |     368 B |        1.00 |
 Command_Struct | 320.4 ns | 0.91 ns | 0.85 ns |  0.86 | 0.0439 |     368 B |        1.00 |
 Query_Class    | 393.6 ns | 4.50 ns | 4.21 ns |  1.06 | 0.0782 |     656 B |        1.78 |
 Query_Struct   | 324.0 ns | 2.48 ns | 2.32 ns |  0.87 | 0.0439 |     368 B |        1.00 |
