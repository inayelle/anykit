
BenchmarkDotNet v0.14.0, Arch Linux
AMD Ryzen 9 6900HX with Radeon Graphics, 1 CPU, 16 logical and 8 physical cores
.NET SDK 8.0.203
  [Host]     : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2
  DefaultJob : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2


 Method         | Mean     | Error   | StdDev  | Ratio | Gen0   | Allocated | Alloc Ratio |
--------------- |---------:|--------:|--------:|------:|-------:|----------:|------------:|
 Command_Class  | 334.7 ns | 2.07 ns | 1.93 ns |  1.00 | 0.0439 |     368 B |        1.00 |
 Command_Struct | 305.4 ns | 2.76 ns | 2.58 ns |  0.91 | 0.0439 |     368 B |        1.00 |
 Query_Class    | 384.0 ns | 3.74 ns | 3.50 ns |  1.15 | 0.0782 |     656 B |        1.78 |
 Query_Struct   | 296.7 ns | 1.64 ns | 1.54 ns |  0.89 | 0.0439 |     368 B |        1.00 |
