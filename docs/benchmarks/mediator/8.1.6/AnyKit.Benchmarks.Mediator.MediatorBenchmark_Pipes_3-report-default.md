
BenchmarkDotNet v0.14.0, Arch Linux
AMD Ryzen 9 6900HX with Radeon Graphics, 1 CPU, 16 logical and 8 physical cores
.NET SDK 8.0.203
  [Host]     : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2
  DefaultJob : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2


 Method         | Mean     | Error   | StdDev  | Ratio | Gen0   | Allocated | Alloc Ratio |
--------------- |---------:|--------:|--------:|------:|-------:|----------:|------------:|
 Command_Class  | 361.1 ns | 1.52 ns | 1.35 ns |  1.00 | 0.0534 |     448 B |        1.00 |
 Command_Struct | 331.1 ns | 0.92 ns | 0.81 ns |  0.92 | 0.0534 |     448 B |        1.00 |
 Query_Class    | 436.6 ns | 4.30 ns | 4.02 ns |  1.21 | 0.0877 |     736 B |        1.64 |
 Query_Struct   | 327.4 ns | 1.09 ns | 0.91 ns |  0.91 | 0.0534 |     448 B |        1.00 |
