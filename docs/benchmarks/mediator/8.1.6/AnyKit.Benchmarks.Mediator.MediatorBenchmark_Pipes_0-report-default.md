
BenchmarkDotNet v0.14.0, Arch Linux
AMD Ryzen 9 6900HX with Radeon Graphics, 1 CPU, 16 logical and 8 physical cores
.NET SDK 8.0.203
  [Host]     : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2
  DefaultJob : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2


 Method         | Mean     | Error   | StdDev  | Ratio | RatioSD | Gen0   | Allocated | Alloc Ratio |
--------------- |---------:|--------:|--------:|------:|--------:|-------:|----------:|------------:|
 Command_Class  | 366.0 ns | 1.69 ns | 1.58 ns |  1.00 |    0.01 | 0.0534 |     448 B |        1.00 |
 Command_Struct | 331.8 ns | 1.78 ns | 1.67 ns |  0.91 |    0.01 | 0.0534 |     448 B |        1.00 |
 Query_Class    | 432.4 ns | 6.30 ns | 5.90 ns |  1.18 |    0.02 | 0.0877 |     736 B |        1.64 |
 Query_Struct   | 326.0 ns | 4.22 ns | 3.95 ns |  0.89 |    0.01 | 0.0534 |     448 B |        1.00 |
