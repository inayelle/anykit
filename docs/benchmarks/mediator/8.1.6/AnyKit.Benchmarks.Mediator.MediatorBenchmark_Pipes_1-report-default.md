
BenchmarkDotNet v0.14.0, Arch Linux
AMD Ryzen 9 6900HX with Radeon Graphics, 1 CPU, 16 logical and 8 physical cores
.NET SDK 8.0.203
  [Host]     : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2
  DefaultJob : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2


 Method         | Mean     | Error   | StdDev  | Ratio | Gen0   | Allocated | Alloc Ratio |
--------------- |---------:|--------:|--------:|------:|-------:|----------:|------------:|
 Command_Class  | 375.4 ns | 2.28 ns | 2.02 ns |  1.00 | 0.0534 |     448 B |        1.00 |
 Command_Struct | 346.7 ns | 1.76 ns | 1.56 ns |  0.92 | 0.0534 |     448 B |        1.00 |
 Query_Class    | 424.9 ns | 2.51 ns | 2.23 ns |  1.13 | 0.0877 |     736 B |        1.64 |
 Query_Struct   | 319.6 ns | 0.82 ns | 0.77 ns |  0.85 | 0.0534 |     448 B |        1.00 |
