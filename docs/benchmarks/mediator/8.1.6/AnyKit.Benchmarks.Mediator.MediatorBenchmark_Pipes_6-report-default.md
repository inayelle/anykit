
BenchmarkDotNet v0.14.0, Arch Linux
AMD Ryzen 9 6900HX with Radeon Graphics, 1 CPU, 16 logical and 8 physical cores
.NET SDK 8.0.203
  [Host]     : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2
  DefaultJob : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2


 Method         | Mean     | Error   | StdDev  | Ratio | Gen0   | Allocated | Alloc Ratio |
--------------- |---------:|--------:|--------:|------:|-------:|----------:|------------:|
 Command_Class  | 375.0 ns | 2.04 ns | 1.81 ns |  1.00 | 0.0534 |     448 B |        1.00 |
 Command_Struct | 340.6 ns | 1.41 ns | 1.32 ns |  0.91 | 0.0534 |     448 B |        1.00 |
 Query_Class    | 439.4 ns | 2.99 ns | 2.80 ns |  1.17 | 0.0877 |     736 B |        1.64 |
 Query_Struct   | 339.6 ns | 3.75 ns | 3.51 ns |  0.91 | 0.0534 |     448 B |        1.00 |
