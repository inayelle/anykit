
BenchmarkDotNet v0.14.0, Arch Linux
AMD Ryzen 9 6900HX with Radeon Graphics, 1 CPU, 16 logical and 8 physical cores
.NET SDK 8.0.203
  [Host]     : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2
  DefaultJob : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2


 Method         | Mean     | Error   | StdDev  | Ratio | Gen0   | Allocated | Alloc Ratio |
--------------- |---------:|--------:|--------:|------:|-------:|----------:|------------:|
 Command_Class  | 426.6 ns | 4.60 ns | 4.30 ns |  1.00 | 0.0782 |     656 B |        1.00 |
 Command_Struct | 398.6 ns | 5.44 ns | 5.08 ns |  0.93 | 0.0782 |     656 B |        1.00 |
 Query_Class    | 396.0 ns | 4.65 ns | 4.35 ns |  0.93 | 0.0782 |     656 B |        1.00 |
 Query_Struct   | 331.7 ns | 0.80 ns | 0.67 ns |  0.78 | 0.0439 |     368 B |        0.56 |
