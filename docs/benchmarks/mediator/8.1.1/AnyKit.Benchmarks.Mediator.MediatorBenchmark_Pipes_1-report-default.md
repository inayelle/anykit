
BenchmarkDotNet v0.14.0, Arch Linux
AMD Ryzen 9 6900HX with Radeon Graphics, 1 CPU, 16 logical and 8 physical cores
.NET SDK 8.0.203
  [Host]     : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2
  DefaultJob : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2


 Method         | Mean     | Error   | StdDev  | Ratio | RatioSD | Gen0   | Allocated | Alloc Ratio |
--------------- |---------:|--------:|--------:|------:|--------:|-------:|----------:|------------:|
 Command_Class  | 432.7 ns | 5.45 ns | 5.10 ns |  1.00 |    0.02 | 0.0782 |     656 B |        1.00 |
 Command_Struct | 397.9 ns | 4.66 ns | 4.36 ns |  0.92 |    0.01 | 0.0782 |     656 B |        1.00 |
 Query_Class    | 396.8 ns | 3.46 ns | 3.07 ns |  0.92 |    0.01 | 0.0782 |     656 B |        1.00 |
 Query_Struct   | 317.8 ns | 2.27 ns | 2.12 ns |  0.73 |    0.01 | 0.0439 |     368 B |        0.56 |
