
BenchmarkDotNet v0.14.0, Arch Linux
AMD Ryzen 9 6900HX with Radeon Graphics, 1 CPU, 16 logical and 8 physical cores
.NET SDK 8.0.203
  [Host]     : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2
  DefaultJob : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2


 Method         | Mean     | Error   | StdDev  | Ratio | Gen0   | Allocated | Alloc Ratio |
--------------- |---------:|--------:|--------:|------:|-------:|----------:|------------:|
 Command_Class  | 412.1 ns | 4.43 ns | 3.93 ns |  1.00 | 0.0782 |     656 B |        1.00 |
 Command_Struct | 415.2 ns | 4.16 ns | 3.89 ns |  1.01 | 0.0782 |     656 B |        1.00 |
 Query_Class    | 398.1 ns | 4.34 ns | 3.85 ns |  0.97 | 0.0782 |     656 B |        1.00 |
 Query_Struct   | 320.6 ns | 3.87 ns | 3.23 ns |  0.78 | 0.0439 |     368 B |        0.56 |
