
BenchmarkDotNet v0.14.0, Arch Linux
AMD Ryzen 9 6900HX with Radeon Graphics, 1 CPU, 16 logical and 8 physical cores
.NET SDK 8.0.203
  [Host]     : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2
  DefaultJob : .NET 8.0.3 (8.0.324.11423), X64 RyuJIT AVX2


 Method         | Mean     | Error   | StdDev  | Ratio | Gen0   | Allocated | Alloc Ratio |
--------------- |---------:|--------:|--------:|------:|-------:|----------:|------------:|
 Command_Class  | 443.0 ns | 5.30 ns | 4.70 ns |  1.00 | 0.0782 |     656 B |        1.00 |
 Command_Struct | 407.8 ns | 1.94 ns | 1.72 ns |  0.92 | 0.0782 |     656 B |        1.00 |
 Query_Class    | 396.1 ns | 2.87 ns | 2.54 ns |  0.89 | 0.0782 |     656 B |        1.00 |
 Query_Struct   | 312.5 ns | 1.08 ns | 0.90 ns |  0.71 | 0.0439 |     368 B |        0.56 |
