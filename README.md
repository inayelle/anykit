# AnyKit

AnyKit is a micro framework that provides a simple yet useful toolset designed to address common software development challenges with ease and efficiency.

## Components

### [AnyKit.Pipelines](https://www.nuget.org/packages/AnyKit.Pipelines/)
Pipelines built on .NET delegates with support of synchronous/asynchronous operations and results.

Packages:
- [![AnyKit.Pipelines](https://img.shields.io/nuget/v/AnyKit.Pipelines?label=AnyKit.Pipelines)](https://www.nuget.org/packages/AnyKit.Pipelines/)
- [![AnyKit.Pipelines.Abstractions](https://img.shields.io/nuget/v/AnyKit.Pipelines.Abstractions?label=AnyKit.Pipelines.Abstractions)](https://www.nuget.org/packages/AnyKit.Pipelines.Abstractions/)

### [AnyKit.Mediator](https://www.nuget.org/packages/AnyKit.Mediator/)
Mediator built on top of [AnyKit.Pipelines](https://www.nuget.org/packages/AnyKit.Pipelines/). Supports sending/publication, struct messages, per-handler pipeline customization, and rich extensibility.

Look for examples in the [examples project](https://gitlab.com/inayelle/anykit/-/tree/master/examples/AnyKit.Examples.Mediator).

Packages:
- [![AnyKit.Mediator](https://img.shields.io/nuget/v/AnyKit.Mediator?label=AnyKit.Mediator)](https://www.nuget.org/packages/AnyKit.Mediator/)
- [![AnyKit.Mediator.Abstractions](https://img.shields.io/nuget/v/AnyKit.Mediator.Abstractions?label=AnyKit.Mediator.Abstractions)](https://www.nuget.org/packages/AnyKit.Mediator.Abstractions/)
- [![AnyKit.Mediator.Extensions](https://img.shields.io/nuget/v/AnyKit.Mediator.Extensions?label=AnyKit.Mediator.Extensions)](https://www.nuget.org/packages/AnyKit.Mediator.Extensions/)

### [AnyKit.StringTemplate](https://www.nuget.org/packages/AnyKit.StringTemplate/)
Straightforward string template compiler.

Packages:
- [![AnyKit.StringTemplate](https://img.shields.io/nuget/v/AnyKit.StringTemplate?label=AnyKit.StringTemplate)](https://www.nuget.org/packages/AnyKit.StringTemplate/)

## Installation
No special rules apply here. Install just like any other package in the .NET ecosystem.

## Contribution
Feel free to open a merge request or an issue.