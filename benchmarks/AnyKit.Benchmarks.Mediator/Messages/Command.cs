using AnyKit.Mediator;

namespace AnyKit.Benchmarks.Mediator.Messages;

internal static class Command
{
    public sealed class Class;
    public readonly struct Struct;

    public static Class ClassInstance { get; } = new();
    public static Struct StructInstance { get; } = new();

    public sealed class Handler
        : IMessageHandler<Class>,
          IMessageHandler<Struct>
    {
        public Task Handle(IExchangeContext<Class> context)
        {
            return Task.CompletedTask;
        }

        public Task Handle(IExchangeContext<Struct> context)
        {
            return Task.CompletedTask;
        }
    }
}