using AnyKit.Mediator;

namespace AnyKit.Benchmarks.Mediator.Messages;

internal static class Query
{
    public sealed class Class;
    public readonly struct Struct;

    public static Class ClassInstance { get; } = new();
    public static Struct StructInstance { get; } = new();

    public sealed class Handler
        : IMessageHandler<Class, Class>,
          IMessageHandler<Struct, Struct>
    {
        public Task<Class> Handle(IExchangeContext<Class> context)
        {
            return Task.FromResult(ClassInstance);
        }

        public Task<Struct> Handle(IExchangeContext<Struct> context)
        {
            return Task.FromResult(StructInstance);
        }
    }
}