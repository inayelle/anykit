﻿using System.Reflection;
using BenchmarkDotNet.Columns;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Diagnosers;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Loggers;
using BenchmarkDotNet.Running;

var config = new ManualConfig();

config.AddColumnProvider(DefaultColumnProviders.Instance);
config.AddLogger(ConsoleLogger.Unicode);
config.AddExporter(MarkdownExporter.Default);
config.AddDiagnoser(MemoryDiagnoser.Default);
config.ArtifactsPath = $"./BenchmarkDotNet.Artifacts/run-{DateTime.Now:yy-MM-dd_hh_mm_ss}";

BenchmarkRunner.Run(Assembly.GetExecutingAssembly(), config, args);