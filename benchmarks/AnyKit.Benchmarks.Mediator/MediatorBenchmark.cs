using System.Reflection;
using AnyKit.Benchmarks.Mediator.Messages;
using AnyKit.Mediator;
using BenchmarkDotNet.Attributes;
using Microsoft.Extensions.DependencyInjection;

namespace AnyKit.Benchmarks.Mediator;

public abstract class MediatorBenchmark
{
    private ServiceProvider _serviceProvider;
    private IMediator _mediator;

    [GlobalSetup]
    public void Setup()
    {
        var services = new ServiceCollection();

        services.AddMediator(builder =>
            {
                builder.AddMessageHandlers(Assembly.GetExecutingAssembly());

                ConfigureMediator(builder);
            }
        );

        _serviceProvider = services.BuildServiceProvider();

        _mediator = _serviceProvider.GetRequiredService<IMediator>();
    }

    [GlobalCleanup]
    public void Cleanup()
    {
        _serviceProvider.Dispose();
    }

    [Benchmark(Baseline = true)]
    public async Task Command_Class()
    {
        await _mediator.Send(Command.ClassInstance);
    }

    [Benchmark]
    public async Task Command_Struct()
    {
        await _mediator.Send(Command.StructInstance);
    }

    [Benchmark]
    public async Task Query_Class()
    {
        await _mediator.Send<Query.Class, Query.Class>(Query.ClassInstance);
    }

    [Benchmark]
    public async Task Query_Struct()
    {
        await _mediator.Send<Query.Struct, Query.Struct>(Query.StructInstance);
    }

    protected abstract void ConfigureMediator(MediatorBuilder builder);
}