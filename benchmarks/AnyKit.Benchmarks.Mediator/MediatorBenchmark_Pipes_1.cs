using AnyKit.Benchmarks.Mediator.Pipes;
using AnyKit.Mediator;

namespace AnyKit.Benchmarks.Mediator;

public class MediatorBenchmark_Pipes_1 : MediatorBenchmark
{
    protected override void ConfigureMediator(MediatorBuilder builder)
    {
        builder.UsePipe(new DummyPipe());
    }
}