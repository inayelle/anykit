using AnyKit.Mediator;
using AnyKit.Pipelines;

namespace AnyKit.Benchmarks.Mediator.Pipes;

internal sealed class DummyPipe : IExchangePipe
{
    public Task<TResult> Invoke<TMessage, TResult>(
        IExchangeContext<TMessage> context,
        AsyncPipeline<IExchangeContext<TMessage>, TResult> next
    )
    {
        return next(context);
    }
}