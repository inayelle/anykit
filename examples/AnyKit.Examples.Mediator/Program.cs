﻿using System.Reflection;
using AnyKit.Examples.Mediator.ShowCases;
using AnyKit.Mediator;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

var services = new ServiceCollection();

services.AddLogging(logging => logging.SetMinimumLevel(LogLevel.Debug).AddConsole());

services.AddMediator(mediator =>
    {
        // add message handlers from the current assembly
        mediator.AddMessageHandlers(Assembly.GetExecutingAssembly());

        // instance pipes shared across all exchanges
        mediator.UsePipe(new SharedInstancePipe0());
        mediator.UsePipe(new SharedInstancePipe1());

        // activated pipes shared across all exchanges
        mediator.UsePipe<SharedActivatedPipe0>();
        mediator.UsePipe<SharedActivatedPipe1>();
    }
);

services
   .AddSingleton<ComplexCommandShowCase>()
   .AddSingleton<ComplexQueryShowCase>()
   .AddSingleton<PlainCommandShowCase>()
   .AddSingleton<PlainEventShowCase>()
   .AddSingleton<PlainQueryShowCase>()
   .AddSingleton<PrimitiveQueryShowCase>()
   .AddSingleton<ConfiguredCommandShowCase>()
   .AddSingleton<StructQueryShowCase>()
   .AddSingleton<MultiMessageShowCase>();

await using var serviceProvider = services.BuildServiceProvider();

// select and execute a showcase
var showCase = serviceProvider.GetRequiredService<MultiMessageShowCase>();
await showCase.Execute();

// Block the main control flow until user presses any key.
// It's here to allow background event handlers to complete handling before the program exits.
Console.WriteLine("Press any key to exit or wait for the remaining handlers.");
await Task.Run(Console.ReadKey);