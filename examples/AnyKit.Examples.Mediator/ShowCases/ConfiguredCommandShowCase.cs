using AnyKit.Examples.Mediator.Pipes;
using AnyKit.Mediator;
using Microsoft.Extensions.Logging;

namespace AnyKit.Examples.Mediator.ShowCases;

internal sealed class ConfiguredCommandShowCase : IShowCase
{
    private readonly IMediator _mediator;

    public ConfiguredCommandShowCase(IMediator mediator)
    {
        _mediator = mediator;
    }

    public async Task Execute()
    {
        await _mediator.Send(new ConfiguredCommand(20));
    }
}

internal sealed class ConfiguredCommand
{
    public int Value { get; }

    public ConfiguredCommand(int value)
    {
        Value = value;
    }
}

internal sealed class ConfiguredCommandHandler : IMessageHandler<ConfiguredCommand>
{
    private readonly ILogger<ConfiguredCommandHandler> _logger;

    public ConfiguredCommandHandler(ILogger<ConfiguredCommandHandler> logger)
    {
        _logger = logger;
    }

    public async Task Handle(IExchangeContext<ConfiguredCommand> context)
    {
        await Task.Delay(100, context.CancellationToken);

        _logger.LogInformation("Executed.");
    }
}

internal sealed class ConfiguredCommandHandlerConfigurator
    : IExchangeConfigurator<ConfiguredCommandHandler, ConfiguredCommand>
{
    public void Configure(IExchangeBuilder<ConfiguredCommand, None> builder)
    {
        builder.UsePipe<DelayPipe>();
    }
}