using AnyKit.Mediator;
using Microsoft.Extensions.Logging;

namespace AnyKit.Examples.Mediator.ShowCases;

internal sealed class PlainQueryShowCase : IShowCase
{
    private readonly IMediator _mediator;

    public PlainQueryShowCase(IMediator mediator)
    {
        _mediator = mediator;
    }

    public async Task Execute()
    {
        var result = await _mediator.Send<PlainQuery, PlainResult>(new PlainQuery(10, 20));
    }
}

internal sealed class PlainQuery
{
    public int ValueA { get; }
    public int ValueB { get; }

    public PlainQuery(int valueA, int valueB)
    {
        ValueA = valueA;
        ValueB = valueB;
    }
}

internal sealed class PlainResult
{
    public int Sum { get; }

    public PlainResult(int sum)
    {
        Sum = sum;
    }
}

internal sealed class PlainQueryHandler : IMessageHandler<PlainQuery, PlainResult>
{
    private readonly ILogger<PlainQueryHandler> _logger;

    public PlainQueryHandler(ILogger<PlainQueryHandler> logger)
    {
        _logger = logger;
    }

    public async Task<PlainResult> Handle(IExchangeContext<PlainQuery> context)
    {
        await Task.Delay(100, context.CancellationToken);

        var sum = context.Message.ValueA + context.Message.ValueB;

        _logger.LogInformation("Executed.");

        return new PlainResult(sum);
    }
}