using AnyKit.Examples.Mediator.Pipes;
using AnyKit.Mediator;
using Microsoft.Extensions.Logging;

namespace AnyKit.Examples.Mediator.ShowCases;

internal sealed class MultiMessageShowCase : IShowCase
{
    private readonly IMediator _mediator;

    public MultiMessageShowCase(IMediator mediator)
    {
        _mediator = mediator;
    }

    public async Task Execute()
    {
        _ = await _mediator.Send<MultiMessageInt16, float>(new MultiMessageInt16(16));
        _ = await _mediator.Send<MultiMessageInt32, double>(new MultiMessageInt32(32));
        _ = await _mediator.Send<MultiMessageInt64, decimal>(new MultiMessageInt64(64));
    }
}

internal sealed class MultiMessageInt16
{
    public Int16 Value { get; }

    public MultiMessageInt16(short value)
    {
        Value = value;
    }
}

internal sealed class MultiMessageInt32
{
    public Int32 Value { get; }

    public MultiMessageInt32(int value)
    {
        Value = value;
    }
}

internal sealed class MultiMessageInt64
{
    public Int64 Value { get; }

    public MultiMessageInt64(long value)
    {
        Value = value;
    }
}

internal sealed class MultiMessageHandler
    : IMessageHandler<MultiMessageInt16, float>,
      IMessageHandler<MultiMessageInt32, double>,
      IMessageHandler<MultiMessageInt64, decimal>
{
    private readonly ILogger<MultiMessageHandler> _logger;

    public MultiMessageHandler(ILogger<MultiMessageHandler> logger)
    {
        _logger = logger;
    }

    public Task<float> Handle(IExchangeContext<MultiMessageInt16> context)
    {
        _logger.LogInformation("Executing for type {Type}", nameof(MultiMessageInt16));
        return Task.FromResult(context.Message.Value * 2f);
    }

    public Task<double> Handle(IExchangeContext<MultiMessageInt32> context)
    {
        _logger.LogInformation("Executing for type {Type}", nameof(MultiMessageInt32));
        return Task.FromResult(context.Message.Value * 2d);
    }

    public Task<decimal> Handle(IExchangeContext<MultiMessageInt64> context)
    {
        _logger.LogInformation("Executing for type {Type}", nameof(MultiMessageInt64));
        return Task.FromResult(context.Message.Value * 2m);
    }
}

internal sealed class MultiMessageExchangeConfigurator
    : IExchangeConfigurator<MultiMessageHandler, MultiMessageInt16, float>,
      IExchangeConfigurator<MultiMessageHandler, MultiMessageInt32, double>,
      IExchangeConfigurator<MultiMessageHandler, MultiMessageInt64, decimal>
{
    public void Configure(IExchangeBuilder<MultiMessageInt16, float> builder)
    {
        builder.UsePipe(new LogPipe("Text for Int16"));
    }

    public void Configure(IExchangeBuilder<MultiMessageInt32, double> builder)
    {
        builder.UsePipe(new LogPipe("Text for Int32"));
    }

    public void Configure(IExchangeBuilder<MultiMessageInt64, decimal> builder)
    {
        builder.UsePipe(new LogPipe("Text for Int64"));
    }
}