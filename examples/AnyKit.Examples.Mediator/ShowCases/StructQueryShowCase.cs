using AnyKit.Mediator;
using Microsoft.Extensions.Logging;

namespace AnyKit.Examples.Mediator.ShowCases;

internal sealed class StructQueryShowCase : IShowCase
{
    private readonly IMediator _mediator;

    public StructQueryShowCase(IMediator mediator)
    {
        _mediator = mediator;
    }

    public async Task Execute()
    {
        var result = await _mediator.Send<StructQuery, StructResult>(new StructQuery(10, 20));
    }
}

internal readonly struct StructQuery
{
    public int ValueA { get; }
    public int ValueB { get; }

    public StructQuery(int valueA, int valueB)
    {
        ValueA = valueA;
        ValueB = valueB;
    }
}

internal readonly struct StructResult
{
    public int Sum { get; }

    public StructResult(int sum)
    {
        Sum = sum;
    }
}

internal sealed class StructQueryHandler : IMessageHandler<StructQuery, StructResult>
{
    private readonly ILogger<StructQueryHandler> _logger;

    public StructQueryHandler(ILogger<StructQueryHandler> logger)
    {
        _logger = logger;
    }

    public async Task<StructResult> Handle(IExchangeContext<StructQuery> context)
    {
        await Task.Delay(100, context.CancellationToken);

        var sum = context.Message.ValueA + context.Message.ValueB;

        _logger.LogInformation("Executed.");

        return new StructResult(sum);
    }
}