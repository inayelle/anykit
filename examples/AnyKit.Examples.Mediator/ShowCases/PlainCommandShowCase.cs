using AnyKit.Mediator;
using Microsoft.Extensions.Logging;

namespace AnyKit.Examples.Mediator.ShowCases;

internal sealed class PlainCommandShowCase : IShowCase
{
    private readonly IMediator _mediator;

    public PlainCommandShowCase(IMediator mediator)
    {
        _mediator = mediator;
    }

    public async Task Execute()
    {
        await _mediator.Send(new PlainCommand(20));
    }
}

internal sealed class PlainCommand
{
    public int Value { get; }

    public PlainCommand(int value)
    {
        Value = value;
    }
}

internal sealed class PlainCommandHandler : IMessageHandler<PlainCommand>
{
    private readonly ILogger<PlainCommandHandler> _logger;

    public PlainCommandHandler(ILogger<PlainCommandHandler> logger)
    {
        _logger = logger;
    }

    public async Task Handle(IExchangeContext<PlainCommand> context)
    {
        await Task.Delay(100, context.CancellationToken);

        _logger.LogInformation("Executed.");
    }
}