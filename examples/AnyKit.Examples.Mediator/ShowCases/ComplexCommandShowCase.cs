using AnyKit.Mediator;
using Microsoft.Extensions.Logging;

namespace AnyKit.Examples.Mediator.ShowCases;

internal sealed class ComplexCommandShowCase : IShowCase
{
    private readonly IMediator _mediator;

    public ComplexCommandShowCase(IMediator mediator)
    {
        _mediator = mediator;
    }

    public async Task Execute()
    {
        await _mediator.Send(new ComplexCommand(20));
    }
}

internal sealed class ComplexCommand
{
    public int Value { get; }

    public ComplexCommand(int value)
    {
        Value = value;
    }
}

internal sealed class ComplexCommandHandler : IMessageHandler<ComplexCommand>
{
    private readonly ILogger<ComplexCommandHandler> _logger;

    public ComplexCommandHandler(ILogger<ComplexCommandHandler> logger)
    {
        _logger = logger;
    }

    public async Task Handle(IExchangeContext<ComplexCommand> context)
    {
        var value = context.Message.Value * 2;

        await context.Publish(new PlainEvent(value));

        _logger.LogInformation("Executed.");
    }
}