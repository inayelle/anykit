namespace AnyKit.Examples.Mediator.ShowCases;

internal interface IShowCase
{
    Task Execute();
}