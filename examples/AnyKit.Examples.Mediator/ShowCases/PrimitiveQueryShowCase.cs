using AnyKit.Mediator;
using Microsoft.Extensions.Logging;

namespace AnyKit.Examples.Mediator.ShowCases;

internal sealed class PrimitiveQueryShowCase : IShowCase
{
    private readonly IMediator _mediator;

    public PrimitiveQueryShowCase(IMediator mediator)
    {
        _mediator = mediator;
    }

    public async Task Execute()
    {
        var result = await _mediator.Send<int, int>(10);
    }
}

internal sealed class PrimitiveQueryHandler : IMessageHandler<int, int>
{
    private readonly ILogger<PrimitiveQueryHandler> _logger;

    public PrimitiveQueryHandler(ILogger<PrimitiveQueryHandler> logger)
    {
        _logger = logger;
    }

    public Task<int> Handle(IExchangeContext<int> context)
    {
        var result = context.Message * 2;

        _logger.LogInformation("Executed.");

        return Task.FromResult(result);
    }
}