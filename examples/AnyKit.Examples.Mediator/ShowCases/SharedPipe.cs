using AnyKit.Mediator;
using AnyKit.Pipelines;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace AnyKit.Examples.Mediator.ShowCases;

internal abstract class SharedPipe : IExchangePipe
{
    public Task<TResult> Invoke<TMessage, TResult>(
        IExchangeContext<TMessage> context,
        AsyncPipeline<IExchangeContext<TMessage>, TResult> next
    )
    {
        var logger = context.ServiceProvider.GetRequiredService<ILoggerFactory>().CreateLogger(GetType().FullName!);

        logger.LogInformation("Executed.");

        return next(context);
    }
}

internal sealed class SharedInstancePipe0 : SharedPipe;
internal sealed class SharedInstancePipe1 : SharedPipe;
internal sealed class SharedActivatedPipe0 : SharedPipe;
internal sealed class SharedActivatedPipe1 : SharedPipe;