using AnyKit.Mediator;
using Microsoft.Extensions.Logging;

namespace AnyKit.Examples.Mediator.ShowCases;

internal sealed class PlainEventShowCase : IShowCase
{
    private readonly IMediator _mediator;

    public PlainEventShowCase(IMediator mediator)
    {
        _mediator = mediator;
    }

    public async Task Execute()
    {
        await _mediator.Publish(new PlainEvent(20));
    }
}

internal sealed class PlainEvent
{
    public int Value { get; }

    public PlainEvent(int value)
    {
        Value = value;
    }
}

internal abstract class PlainEventHandler : IMessageHandler<PlainEvent>
{
    private readonly ILogger _logger;
    private readonly TimeSpan _delay;

    protected PlainEventHandler(ILogger logger, TimeSpan delay)
    {
        _logger = logger;
        _delay = delay;
    }

    public async Task Handle(IExchangeContext<PlainEvent> context)
    {
        await Task.Delay(_delay, context.CancellationToken);

        _logger.LogInformation("Executed.");
    }
}

internal sealed class PlainEventHandler0 : PlainEventHandler
{
    public PlainEventHandler0(ILogger<PlainEventHandler0> logger) : base(logger, TimeSpan.FromSeconds(1))
    {
    }
}

internal sealed class PlainEventHandler1 : PlainEventHandler
{
    public PlainEventHandler1(ILogger<PlainEventHandler1> logger) : base(logger, TimeSpan.FromSeconds(2))
    {
    }
}

internal sealed class PlainEventHandler2 : PlainEventHandler
{
    public PlainEventHandler2(ILogger<PlainEventHandler2> logger) : base(logger, TimeSpan.FromSeconds(3))
    {
    }
}