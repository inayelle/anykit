using AnyKit.Mediator;
using Microsoft.Extensions.Logging;

namespace AnyKit.Examples.Mediator.ShowCases;

internal sealed class ComplexQueryShowCase : IShowCase
{
    private readonly IMediator _mediator;

    public ComplexQueryShowCase(IMediator mediator)
    {
        _mediator = mediator;
    }

    public async Task Execute()
    {
        var result = await _mediator.Send<ComplexQuery, ComplexResult>(new ComplexQuery(10, 20));
    }
}

internal sealed class ComplexQuery
{
    public int ValueA { get; }
    public int ValueB { get; }

    public ComplexQuery(int valueA, int valueB)
    {
        ValueA = valueA;
        ValueB = valueB;
    }
}

internal sealed class ComplexResult
{
    public int SumSquared { get; }

    public ComplexResult(int sumSquared)
    {
        SumSquared = sumSquared;
    }
}

internal sealed class ComplexQueryHandler : IMessageHandler<ComplexQuery, ComplexResult>
{
    private readonly ILogger<ComplexQueryHandler> _logger;

    public ComplexQueryHandler(ILogger<ComplexQueryHandler> logger)
    {
        _logger = logger;
    }

    public async Task<ComplexResult> Handle(IExchangeContext<ComplexQuery> context)
    {
        var (a, b) = (context.Message.ValueA, context.Message.ValueB);

        var sumResult = await context.Send<PlainQuery, PlainResult>(new PlainQuery(a, b));

        var squaredSum = (int)Math.Pow(sumResult.Sum, 2);

        _logger.LogInformation("Executed.");

        return new ComplexResult(squaredSum);
    }
}