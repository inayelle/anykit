using AnyKit.Mediator;
using AnyKit.Pipelines;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace AnyKit.Examples.Mediator.Pipes;

internal sealed class LogPipe : IExchangePipe
{
    private readonly string _text;

    public LogPipe(string text)
    {
        _text = text;
    }

    public Task<TResult> Invoke<TMessage, TResult>(
        IExchangeContext<TMessage> context,
        AsyncPipeline<IExchangeContext<TMessage>, TResult> next
    )
    {
        var logger = context.ServiceProvider.GetRequiredService<ILogger<LogPipe>>();

        logger.LogInformation(
            "Text for exchange of ({MessageType}, {ResultType}) is {Text}. Correlation id: {CorrelationId}",
            typeof(TMessage).Name,
            typeof(TResult).Name,
            _text,
            context.CorrelationId
        );

        return next(context);
    }
}