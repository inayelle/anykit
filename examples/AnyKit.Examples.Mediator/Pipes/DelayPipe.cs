using AnyKit.Mediator;
using AnyKit.Pipelines;
using Microsoft.Extensions.Logging;

namespace AnyKit.Examples.Mediator.Pipes;

internal sealed class DelayPipe : IExchangePipe
{
    private readonly ILogger<DelayPipe> _logger;

    public DelayPipe(ILogger<DelayPipe> logger)
    {
        _logger = logger;
    }

    public async Task<TResult> Invoke<TMessage, TResult>(
        IExchangeContext<TMessage> context,
        AsyncPipeline<IExchangeContext<TMessage>, TResult> next
    )
    {
        var delay = TimeSpan.FromMilliseconds(Random.Shared.Next(100, 2000));

        _logger.LogInformation(
            "Delaying exchange of ({MessageType}, {ResultType}) for {Delay}. Correlation id: {CorrelationId}",
            typeof(TMessage).Name,
            typeof(TResult).Name,
            delay,
            context.CorrelationId
        );

        await Task.Delay(delay);

        return await next(context);
    }
}