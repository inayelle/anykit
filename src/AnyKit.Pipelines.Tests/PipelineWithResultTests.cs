using NSubstitute;

namespace AnyKit.Pipelines;

[TestFixture]
[TestOf(typeof(PipelineBuilder<,>))]
internal sealed class PipelineWithResultTests
{
    private PipelineBuilder<PipelineContext, PipelineResult> _pipelineBuilder;

    [SetUp]
    public void SetUp()
    {
        _pipelineBuilder = new PipelineBuilder<PipelineContext, PipelineResult>();
    }

    [Test]
    public void Pipeline_ShouldInvokePipesInOrder_WhenEachPipeInvokesNext()
    {
        // Arrange
        var initialContext = new PipelineContext();
        var expectedResult = new PipelineResult();

        var pipe1 = PipeSubstitute.WithResult.InvokesNext();
        var pipe2 = PipeSubstitute.WithResult.InvokesNext();
        var pipe3 = PipeSubstitute.WithResult.Returns(expectedResult);

        _pipelineBuilder.UsePipe(pipe1);
        _pipelineBuilder.UsePipe(pipe2);
        _pipelineBuilder.UsePipe(pipe3);

        var pipeline = _pipelineBuilder.Build();

        // Act & Assert
        Assert.That(() => pipeline.Invoke(initialContext), Is.EqualTo(expectedResult));

        Received.InOrder(() =>
            {
                pipe1.Received(1).Invoke(initialContext, Arg.Any<Pipeline<PipelineContext, PipelineResult>>());
                pipe2.Received(1).Invoke(initialContext, Arg.Any<Pipeline<PipelineContext, PipelineResult>>());
                pipe3.Received(1).Invoke(initialContext, Arg.Any<Pipeline<PipelineContext, PipelineResult>>());
            }
        );
    }

    [Test]
    public void Pipeline_ShouldStopInvocation_WhenPipeDoesNotInvokeNext()
    {
        // Arrange
        var initialContext = new PipelineContext();
        var expectedResult = new PipelineResult();
        var unreachableResult = new PipelineResult();

        var pipe1 = PipeSubstitute.WithResult.InvokesNext();
        var pipe2 = PipeSubstitute.WithResult.Returns(expectedResult);
        var pipe3 = PipeSubstitute.WithResult.Returns(unreachableResult);

        _pipelineBuilder.UsePipe(pipe1);
        _pipelineBuilder.UsePipe(pipe2);
        _pipelineBuilder.UsePipe(pipe3);

        var pipeline = _pipelineBuilder.Build();

        // Act & Assert
        Assert.That(() => pipeline.Invoke(initialContext), Is.EqualTo(expectedResult));

        Received.InOrder(() =>
            {
                pipe1.Received(1).Invoke(initialContext, Arg.Any<Pipeline<PipelineContext, PipelineResult>>());
                pipe2.Received(1).Invoke(initialContext, Arg.Any<Pipeline<PipelineContext, PipelineResult>>());
            }
        );

        pipe3.Received(0).Invoke(initialContext, Arg.Any<Pipeline<PipelineContext, PipelineResult>>());
    }

    [Test]
    public void Pipeline_ShouldStopInvocation_WhenPipeThrowsException()
    {
        // Arrange
        var initialContext = new PipelineContext();
        var unreachableResult = new PipelineResult();

        var pipe1 = PipeSubstitute.WithResult.InvokesNext();
        var pipe2 = PipeSubstitute.WithResult.Throws(new Exception("Test exception"));
        var pipe3 = PipeSubstitute.WithResult.Returns(unreachableResult);

        _pipelineBuilder.UsePipe(pipe1);
        _pipelineBuilder.UsePipe(pipe2);
        _pipelineBuilder.UsePipe(pipe3);

        var pipeline = _pipelineBuilder.Build();

        // Act & Assert
        Assert.Throws<Exception>(() => pipeline.Invoke(initialContext), "Test exception");

        Received.InOrder(() =>
            {
                pipe1.Received(1).Invoke(initialContext, Arg.Any<Pipeline<PipelineContext, PipelineResult>>());
                pipe2.Received(1).Invoke(initialContext, Arg.Any<Pipeline<PipelineContext, PipelineResult>>());
            }
        );

        pipe3.Received(0).Invoke(initialContext, Arg.Any<Pipeline<PipelineContext, PipelineResult>>());
    }

    [Test]
    public void Pipeline_ShouldInvokeWithNewContext_WhenPipeSwitchesContext()
    {
        // Arrange
        var initialContext = new PipelineContext();
        var latterContext = new PipelineContext();
        var expectedResult = new PipelineResult();

        var pipe1 = PipeSubstitute.WithResult.InvokesNext();
        var pipe2 = PipeSubstitute.WithResult.InvokesNext(latterContext);
        var pipe3 = PipeSubstitute.WithResult.Returns(expectedResult);

        _pipelineBuilder.UsePipe(pipe1);
        _pipelineBuilder.UsePipe(pipe2);
        _pipelineBuilder.UsePipe(pipe3);

        var pipeline = _pipelineBuilder.Build();

        // Act & Assert
        Assert.That(() => pipeline.Invoke(initialContext), Is.EqualTo(expectedResult));

        Received.InOrder(() =>
            {
                pipe1.Received(1).Invoke(initialContext, Arg.Any<Pipeline<PipelineContext, PipelineResult>>());
                pipe2.Received(1).Invoke(initialContext, Arg.Any<Pipeline<PipelineContext, PipelineResult>>());
                pipe3.Received(1).Invoke(latterContext, Arg.Any<Pipeline<PipelineContext, PipelineResult>>());
            }
        );
    }

    [Test]
    public void Pipeline_ShouldReturnDefault_WhenLastPipeDoesNotReturn()
    {
        // Arrange
        var initialContext = new PipelineContext();

        var pipe1 = PipeSubstitute.WithResult.InvokesNext();
        var pipe2 = PipeSubstitute.WithResult.InvokesNext();
        var pipe3 = PipeSubstitute.WithResult.InvokesNext();

        _pipelineBuilder.UsePipe(pipe1);
        _pipelineBuilder.UsePipe(pipe2);
        _pipelineBuilder.UsePipe(pipe3);

        var pipeline = _pipelineBuilder.Build();

        // Act & Assert
        Assert.That(() => pipeline.Invoke(initialContext), Is.Default);

        Received.InOrder(() =>
            {
                pipe1.Received(1).Invoke(initialContext, Arg.Any<Pipeline<PipelineContext, PipelineResult>>());
                pipe2.Received(1).Invoke(initialContext, Arg.Any<Pipeline<PipelineContext, PipelineResult>>());
                pipe3.Received(1).Invoke(initialContext, Arg.Any<Pipeline<PipelineContext, PipelineResult>>());
            }
        );
    }
}