using NSubstitute;

namespace AnyKit.Pipelines;

[TestFixture]
[TestOf(typeof(PipelineBuilder<>))]
internal sealed class PipelineWithoutResultTests
{
    private PipelineBuilder<PipelineContext> _pipelineBuilder;

    [SetUp]
    public void SetUp()
    {
        _pipelineBuilder = new PipelineBuilder<PipelineContext>();
    }

    [Test]
    public void Pipeline_ShouldInvokePipesInOrder_WhenEachPipeInvokesNext()
    {
        // Arrange
        var context = new PipelineContext();

        var pipe1 = PipeSubstitute.WithoutResult.InvokesNext();
        var pipe2 = PipeSubstitute.WithoutResult.InvokesNext();
        var pipe3 = PipeSubstitute.WithoutResult.InvokesNext();

        _pipelineBuilder.UsePipe(pipe1);
        _pipelineBuilder.UsePipe(pipe2);
        _pipelineBuilder.UsePipe(pipe3);

        var pipeline = _pipelineBuilder.Build();

        // Act & Assert
        Assert.DoesNotThrow(() => pipeline.Invoke(context));

        Received.InOrder(() =>
            {
                pipe1.Received(1).Invoke(context, Arg.Any<Pipeline<PipelineContext>>());
                pipe2.Received(1).Invoke(context, Arg.Any<Pipeline<PipelineContext>>());
                pipe3.Received(1).Invoke(context, Arg.Any<Pipeline<PipelineContext>>());
            }
        );
    }

    [Test]
    public void Pipeline_ShouldStopInvocation_WhenPipeDoesNotInvokeNext()
    {
        // Arrange
        var context = new PipelineContext();

        var pipe1 = PipeSubstitute.WithoutResult.InvokesNext();
        var pipe2 = PipeSubstitute.WithoutResult.Returns();
        var pipe3 = PipeSubstitute.WithoutResult.InvokesNext();

        _pipelineBuilder.UsePipe(pipe1);
        _pipelineBuilder.UsePipe(pipe2);
        _pipelineBuilder.UsePipe(pipe3);

        var pipeline = _pipelineBuilder.Build();

        // Act & Assert
        Assert.DoesNotThrow(() => pipeline.Invoke(context));

        Received.InOrder(() =>
            {
                pipe1.Received(1).Invoke(context, Arg.Any<Pipeline<PipelineContext>>());
                pipe2.Received(1).Invoke(context, Arg.Any<Pipeline<PipelineContext>>());
            }
        );

        pipe3.Received(0).Invoke(context, Arg.Any<Pipeline<PipelineContext>>());
    }

    [Test]
    public void Pipeline_ShouldStopInvocation_WhenPipeThrowsException()
    {
        // Arrange
        var context = new PipelineContext();

        var pipe1 = PipeSubstitute.WithoutResult.InvokesNext();
        var pipe2 = PipeSubstitute.WithoutResult.Throws(new Exception("Test exception"));
        var pipe3 = PipeSubstitute.WithoutResult.InvokesNext();

        _pipelineBuilder.UsePipe(pipe1);
        _pipelineBuilder.UsePipe(pipe2);
        _pipelineBuilder.UsePipe(pipe3);

        var pipeline = _pipelineBuilder.Build();

        // Act & Assert
        Assert.Throws<Exception>(() => pipeline.Invoke(context), "Test exception");

        Received.InOrder(() =>
            {
                pipe1.Received(1).Invoke(context, Arg.Any<Pipeline<PipelineContext>>());
                pipe2.Received(1).Invoke(context, Arg.Any<Pipeline<PipelineContext>>());
            }
        );

        pipe3.Received(0).Invoke(context, Arg.Any<Pipeline<PipelineContext>>());
    }

    [Test]
    public void Pipeline_ShouldInvokeWithNewContext_WhenPipeSwitchesContext()
    {
        // Arrange
        var initialContext = new PipelineContext();
        var latterContext = new PipelineContext();

        var pipe1 = PipeSubstitute.WithoutResult.InvokesNext();
        var pipe2 = PipeSubstitute.WithoutResult.InvokesNext(latterContext);
        var pipe3 = PipeSubstitute.WithoutResult.InvokesNext();

        _pipelineBuilder.UsePipe(pipe1);
        _pipelineBuilder.UsePipe(pipe2);
        _pipelineBuilder.UsePipe(pipe3);

        var pipeline = _pipelineBuilder.Build();

        // Act & Assert
        Assert.DoesNotThrow(() => pipeline.Invoke(initialContext));

        Received.InOrder(() =>
            {
                pipe1.Received(1).Invoke(initialContext, Arg.Any<Pipeline<PipelineContext>>());
                pipe2.Received(1).Invoke(initialContext, Arg.Any<Pipeline<PipelineContext>>());
                pipe3.Received(1).Invoke(latterContext, Arg.Any<Pipeline<PipelineContext>>());
            }
        );
    }
}