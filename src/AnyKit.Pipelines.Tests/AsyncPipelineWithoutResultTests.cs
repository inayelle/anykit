using NSubstitute;

namespace AnyKit.Pipelines;

[TestFixture]
[TestOf(typeof(AsyncPipelineBuilder<>))]
internal sealed class AsyncPipelineWithoutResultTests
{
    private AsyncPipelineBuilder<PipelineContext> _pipelineBuilder;

    [SetUp]
    public void SetUp()
    {
        _pipelineBuilder = new AsyncPipelineBuilder<PipelineContext>();
    }

    [Test]
    public void Pipeline_ShouldInvokePipesInOrder_WhenEachPipeInvokesNext()
    {
        // Arrange
        var context = new PipelineContext();

        var pipe1 = AsyncPipeSubstitute.WithoutResult.InvokesNext();
        var pipe2 = AsyncPipeSubstitute.WithoutResult.InvokesNext();
        var pipe3 = AsyncPipeSubstitute.WithoutResult.InvokesNext();

        _pipelineBuilder.UsePipe(pipe1);
        _pipelineBuilder.UsePipe(pipe2);
        _pipelineBuilder.UsePipe(pipe3);

        var pipeline = _pipelineBuilder.Build();

        // Act & Assert
        Assert.DoesNotThrowAsync(async () => await pipeline.Invoke(context));

        Received.InOrder(() =>
            {
                pipe1.Received(1).Invoke(context, Arg.Any<AsyncPipeline<PipelineContext>>());
                pipe2.Received(1).Invoke(context, Arg.Any<AsyncPipeline<PipelineContext>>());
                pipe3.Received(1).Invoke(context, Arg.Any<AsyncPipeline<PipelineContext>>());
            }
        );
    }

    [Test]
    public void Pipeline_ShouldStopInvocation_WhenPipeDoesNotInvokeNext()
    {
        // Arrange
        var context = new PipelineContext();

        var pipe1 = AsyncPipeSubstitute.WithoutResult.InvokesNext();
        var pipe2 = AsyncPipeSubstitute.WithoutResult.Returns();
        var pipe3 = AsyncPipeSubstitute.WithoutResult.InvokesNext();

        _pipelineBuilder.UsePipe(pipe1);
        _pipelineBuilder.UsePipe(pipe2);
        _pipelineBuilder.UsePipe(pipe3);

        var pipeline = _pipelineBuilder.Build();

        // Act & Assert
        Assert.DoesNotThrowAsync(async () => await pipeline.Invoke(context));

        Received.InOrder(() =>
            {
                pipe1.Received(1).Invoke(context, Arg.Any<AsyncPipeline<PipelineContext>>());
                pipe2.Received(1).Invoke(context, Arg.Any<AsyncPipeline<PipelineContext>>());
            }
        );

        pipe3.Received(0).Invoke(context, Arg.Any<AsyncPipeline<PipelineContext>>());
    }

    [Test]
    public void Pipeline_ShouldStopInvocation_WhenPipeThrowsException()
    {
        // Arrange
        var context = new PipelineContext();

        var pipe1 = AsyncPipeSubstitute.WithoutResult.InvokesNext();
        var pipe2 = AsyncPipeSubstitute.WithoutResult.Throws(new Exception("Test exception"));
        var pipe3 = AsyncPipeSubstitute.WithoutResult.InvokesNext();

        _pipelineBuilder.UsePipe(pipe1);
        _pipelineBuilder.UsePipe(pipe2);
        _pipelineBuilder.UsePipe(pipe3);

        var pipeline = _pipelineBuilder.Build();

        // Act & Assert
        Assert.ThrowsAsync<Exception>(async () => await pipeline.Invoke(context), "Test exception");

        Received.InOrder(() =>
            {
                pipe1.Received(1).Invoke(context, Arg.Any<AsyncPipeline<PipelineContext>>());
                pipe2.Received(1).Invoke(context, Arg.Any<AsyncPipeline<PipelineContext>>());
            }
        );

        pipe3.Received(0).Invoke(context, Arg.Any<AsyncPipeline<PipelineContext>>());
    }

    [Test]
    public void Pipeline_ShouldInvokeWithNewContext_WhenPipeSwitchesContext()
    {
        // Arrange
        var initialContext = new PipelineContext();
        var latterContext = new PipelineContext();

        var pipe1 = AsyncPipeSubstitute.WithoutResult.InvokesNext();
        var pipe2 = AsyncPipeSubstitute.WithoutResult.InvokesNext(latterContext);
        var pipe3 = AsyncPipeSubstitute.WithoutResult.InvokesNext();

        _pipelineBuilder.UsePipe(pipe1);
        _pipelineBuilder.UsePipe(pipe2);
        _pipelineBuilder.UsePipe(pipe3);

        var pipeline = _pipelineBuilder.Build();

        // Act & Assert
        Assert.DoesNotThrowAsync(async () => await pipeline.Invoke(initialContext));

        Received.InOrder(() =>
            {
                pipe1.Received(1).Invoke(initialContext, Arg.Any<AsyncPipeline<PipelineContext>>());
                pipe2.Received(1).Invoke(initialContext, Arg.Any<AsyncPipeline<PipelineContext>>());
                pipe3.Received(1).Invoke(latterContext, Arg.Any<AsyncPipeline<PipelineContext>>());
            }
        );
    }
}