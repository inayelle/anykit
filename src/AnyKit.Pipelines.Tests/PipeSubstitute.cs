using NSubstitute;
using NSubstitute.ExceptionExtensions;

namespace AnyKit.Pipelines;

internal static class PipeSubstitute
{
    public static class WithoutResult
    {
        public static Pipe<PipelineContext> InvokesNext()
        {
            var pipe = Substitute.For<Pipe<PipelineContext>>();

            pipe
               .When(x => x.Invoke(Arg.Any<PipelineContext>(), Arg.Any<Pipeline<PipelineContext>>()))
               .Do(info => info.Arg<Pipeline<PipelineContext>>().Invoke(info.Arg<PipelineContext>()));

            return pipe;
        }

        public static Pipe<PipelineContext> InvokesNext(PipelineContext context)
        {
            var pipe = Substitute.For<Pipe<PipelineContext>>();

            pipe
               .When(x => x.Invoke(Arg.Any<PipelineContext>(), Arg.Any<Pipeline<PipelineContext>>()))
               .Do(info => info.Arg<Pipeline<PipelineContext>>().Invoke(context));

            return pipe;
        }

        public static Pipe<PipelineContext> Returns()
        {
            var pipe = Substitute.For<Pipe<PipelineContext>>();

            pipe
               .When(x => x.Invoke(Arg.Any<PipelineContext>(), Arg.Any<Pipeline<PipelineContext>>()))
               .Do(_ => { });

            return pipe;
        }

        public static Pipe<PipelineContext> Throws(Exception exception)
        {
            var pipe = Substitute.For<Pipe<PipelineContext>>();

            pipe
               .When(info => info.Invoke(Arg.Any<PipelineContext>(), Arg.Any<Pipeline<PipelineContext>>()))
               .Throw(exception);

            return pipe;
        }
    }

    public static class WithResult
    {
        public static Pipe<PipelineContext, PipelineResult> InvokesNext()
        {
            var pipe = Substitute.For<Pipe<PipelineContext, PipelineResult>>();

            pipe
               .Invoke(Arg.Any<PipelineContext>(), Arg.Any<Pipeline<PipelineContext, PipelineResult>>())
               .Returns(x => x.Arg<Pipeline<PipelineContext, PipelineResult>>().Invoke(x.Arg<PipelineContext>()));

            return pipe;
        }

        public static Pipe<PipelineContext, PipelineResult> InvokesNext(PipelineContext context)
        {
            var pipe = Substitute.For<Pipe<PipelineContext, PipelineResult>>();

            pipe
               .Invoke(Arg.Any<PipelineContext>(), Arg.Any<Pipeline<PipelineContext, PipelineResult>>())
               .Returns(x => x.Arg<Pipeline<PipelineContext, PipelineResult>>().Invoke(context));

            return pipe;
        }

        public static Pipe<PipelineContext, PipelineResult> Returns(PipelineResult result)
        {
            var pipe = Substitute.For<Pipe<PipelineContext, PipelineResult>>();

            pipe
               .Invoke(Arg.Any<PipelineContext>(), Arg.Any<Pipeline<PipelineContext, PipelineResult>>())
               .Returns(result);

            return pipe;
        }

        public static Pipe<PipelineContext, PipelineResult> Throws(Exception exception)
        {
            var pipe = Substitute.For<Pipe<PipelineContext, PipelineResult>>();

            pipe
               .Invoke(Arg.Any<PipelineContext>(), Arg.Any<Pipeline<PipelineContext, PipelineResult>>())
               .Throws(exception);

            return pipe;
        }
    }
}