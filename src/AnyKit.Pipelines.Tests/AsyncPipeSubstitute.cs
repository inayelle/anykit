using NSubstitute;
using NSubstitute.ExceptionExtensions;

namespace AnyKit.Pipelines;

internal static class AsyncPipeSubstitute
{
    public static class WithoutResult
    {
        public static AsyncPipe<PipelineContext> InvokesNext()
        {
            var pipe = Substitute.For<AsyncPipe<PipelineContext>>();

            pipe
               .Invoke(Arg.Any<PipelineContext>(), Arg.Any<AsyncPipeline<PipelineContext>>())
               .Returns(info => info.Arg<AsyncPipeline<PipelineContext>>().Invoke(info.Arg<PipelineContext>()));

            return pipe;
        }

        public static AsyncPipe<PipelineContext> InvokesNext(PipelineContext context)
        {
            var pipe = Substitute.For<AsyncPipe<PipelineContext>>();

            pipe
               .Invoke(Arg.Any<PipelineContext>(), Arg.Any<AsyncPipeline<PipelineContext>>())
               .Returns(info => info.Arg<AsyncPipeline<PipelineContext>>().Invoke(context));

            return pipe;
        }

        public static AsyncPipe<PipelineContext> Returns()
        {
            var pipe = Substitute.For<AsyncPipe<PipelineContext>>();

            pipe
               .Invoke(Arg.Any<PipelineContext>(), Arg.Any<AsyncPipeline<PipelineContext>>())
               .Returns(Task.CompletedTask);

            return pipe;
        }

        public static AsyncPipe<PipelineContext> Throws(Exception exception)
        {
            var pipe = Substitute.For<AsyncPipe<PipelineContext>>();

            pipe
               .Invoke(Arg.Any<PipelineContext>(), Arg.Any<AsyncPipeline<PipelineContext>>())
               .Returns(Task.FromException(exception));

            return pipe;
        }
    }

    public static class WithResult
    {
        public static AsyncPipe<PipelineContext, PipelineResult> InvokesNext()
        {
            var pipe = Substitute.For<AsyncPipe<PipelineContext, PipelineResult>>();

            pipe
               .Invoke(Arg.Any<PipelineContext>(), Arg.Any<AsyncPipeline<PipelineContext, PipelineResult>>())
               .Returns(x => x.Arg<AsyncPipeline<PipelineContext, PipelineResult>>().Invoke(x.Arg<PipelineContext>()));

            return pipe;
        }

        public static AsyncPipe<PipelineContext, PipelineResult> InvokesNext(PipelineContext context)
        {
            var pipe = Substitute.For<AsyncPipe<PipelineContext, PipelineResult>>();

            pipe
               .Invoke(Arg.Any<PipelineContext>(), Arg.Any<AsyncPipeline<PipelineContext, PipelineResult>>())
               .Returns(x => x.Arg<AsyncPipeline<PipelineContext, PipelineResult>>().Invoke(context));

            return pipe;
        }

        public static AsyncPipe<PipelineContext, PipelineResult> Returns(PipelineResult result)
        {
            var pipe = Substitute.For<AsyncPipe<PipelineContext, PipelineResult>>();

            pipe
               .Invoke(Arg.Any<PipelineContext>(), Arg.Any<AsyncPipeline<PipelineContext, PipelineResult>>())
               .Returns(Task.FromResult(result));

            return pipe;
        }

        public static AsyncPipe<PipelineContext, PipelineResult> Throws(Exception exception)
        {
            var pipe = Substitute.For<AsyncPipe<PipelineContext, PipelineResult>>();

            pipe
               .Invoke(Arg.Any<PipelineContext>(), Arg.Any<AsyncPipeline<PipelineContext, PipelineResult>>())
               .Returns(Task.FromException<PipelineResult>(exception));

            return pipe;
        }
    }
}