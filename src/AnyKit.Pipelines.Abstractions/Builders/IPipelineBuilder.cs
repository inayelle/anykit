namespace AnyKit.Pipelines;

public interface IPipelineBuilder<TContext>
{
    void UsePipe(Pipe<TContext> pipe);
}

public interface IPipelineBuilder<TContext, TResult>
{
    void UsePipe(Pipe<TContext, TResult> pipe);
}