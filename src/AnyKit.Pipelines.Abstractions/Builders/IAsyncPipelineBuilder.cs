namespace AnyKit.Pipelines;

public interface IAsyncPipelineBuilder<TContext>
{
    void UsePipe(AsyncPipe<TContext> pipe);
}

public interface IAsyncPipelineBuilder<TContext, TResult>
{
    void UsePipe(AsyncPipe<TContext, TResult> pipe);
}