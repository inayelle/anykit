namespace AnyKit.Pipelines;

public delegate void Pipe<TContext>(TContext context, Pipeline<TContext> next);

public delegate TResult Pipe<TContext, TResult>(TContext context, Pipeline<TContext, TResult> next);