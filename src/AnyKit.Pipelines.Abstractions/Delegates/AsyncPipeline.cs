namespace AnyKit.Pipelines;

public delegate Task AsyncPipeline<in TContext>(TContext context);

public delegate Task<TResult> AsyncPipeline<in TContext, TResult>(TContext context);