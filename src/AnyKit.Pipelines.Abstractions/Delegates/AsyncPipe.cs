namespace AnyKit.Pipelines;

public delegate Task AsyncPipe<TContext>(TContext context, AsyncPipeline<TContext> next);

public delegate Task<TResult> AsyncPipe<TContext, TResult>(TContext context, AsyncPipeline<TContext, TResult> next);