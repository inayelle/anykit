namespace AnyKit.Pipelines;

public delegate void Pipeline<in TContext>(TContext context);

public delegate TResult Pipeline<in TContext, out TResult>(TContext context);