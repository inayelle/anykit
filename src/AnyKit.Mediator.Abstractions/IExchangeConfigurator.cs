namespace AnyKit.Mediator;

/// <summary>
/// Configures an exchange for a specific message handler.
/// </summary>
/// <typeparam name="THandler">The type of the message handler being configured.</typeparam>
/// <typeparam name="TMessage">The type of the message being handled.</typeparam>
/// <typeparam name="TResult">The type of the result produced by the handler.</typeparam>
public interface IExchangeConfigurator<THandler, TMessage, TResult>
    where THandler : class, IMessageHandler<TMessage, TResult>
{
    /// <summary>
    /// Configures the exchange using the specified exchange builder.
    /// </summary>
    /// <param name="builder">The exchange builder used to configure the exchange.</param>
    void Configure(IExchangeBuilder<TMessage, TResult> builder);
}

/// <summary>
/// Configures an exchange for a specific message handler without returning a result.
/// It's a shorthand for the <see cref="IExchangeConfigurator{THandler,TMessage,TResult}"/>, where TResult is <see cref="AnyKit.Mediator.None"/>.
/// </summary>
/// <typeparam name="THandler">The type of the message handler being configured.</typeparam>
/// <typeparam name="TMessage">The type of the message being handled.</typeparam>
public interface IExchangeConfigurator<THandler, TMessage> : IExchangeConfigurator<THandler, TMessage, None>
    where THandler : class, IMessageHandler<TMessage, None>
{
}