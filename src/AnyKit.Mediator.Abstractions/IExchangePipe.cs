using AnyKit.Pipelines;

namespace AnyKit.Mediator;

/// <summary>
/// Represents a pipe in an exchange pipeline that processes messages.
/// </summary>
public interface IExchangePipe
{
    /// <summary>
    /// Invokes the pipe with the given context and the remaining pipeline tail.
    /// </summary>
    /// <typeparam name="TMessage">The type of the message being processed.</typeparam>
    /// <typeparam name="TResult">The type of the result produced by the pipe.</typeparam>
    /// <param name="context">The context of the exchange containing the message.</param>
    /// <param name="next">The remaining pipeline tail to be executed.</param>
    /// <returns>A task representing the asynchronous operation, containing the result produced by the pipe.</returns>
    Task<TResult> Invoke<TMessage, TResult>(
        IExchangeContext<TMessage> context,
        AsyncPipeline<IExchangeContext<TMessage>, TResult> next
    );
}

/// <summary>
/// Represents a generic pipe in an exchange pipeline that processes messages.
/// </summary>
/// <typeparam name="TMessage">The type of the message being processed.</typeparam>
/// <typeparam name="TResult">The type of the result produced by the pipe.</typeparam>
public interface IExchangePipe<TMessage, TResult>
{
    /// <summary>
    /// Invokes the pipe with the given context and the remaining pipeline tail.
    /// </summary>
    /// <param name="context">The context of the exchange containing the message.</param>
    /// <param name="next">The remaining pipeline tail to be executed.</param>
    /// <returns>A task representing the asynchronous operation, containing the result produced by the pipe.</returns>
    Task<TResult> Invoke(
        IExchangeContext<TMessage> context,
        AsyncPipeline<IExchangeContext<TMessage>, TResult> next
    );
}