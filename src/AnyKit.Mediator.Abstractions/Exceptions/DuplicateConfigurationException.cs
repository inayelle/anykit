namespace AnyKit.Mediator;

/// <summary>
/// Exception thrown when there's an attempt to register Mediator for the second time.
/// </summary>
public sealed class DuplicateConfigurationException : MediatorException
{
    public DuplicateConfigurationException() : base(SR.DuplicateConfiguration)
    {
    }
}