namespace AnyKit.Mediator;

/// <summary>
/// Exception thrown when an expected exchange is missing for the given message and result types.
/// </summary>
public sealed class MissingExchangeException : MediatorException
{
    public Type MessageType { get; }
    public Type ResultType { get; }

    public MissingExchangeException(Type messageType, Type resultType)
        : base(String.Format(SR.MissingExchange, messageType.FullName, resultType.FullName))
    {
        MessageType = messageType;
        ResultType = resultType;
    }
}