namespace AnyKit.Mediator;

public sealed class HubClosedException : MediatorException
{
    public HubClosedException() : base(SR.HubClosed)
    {
    }
}