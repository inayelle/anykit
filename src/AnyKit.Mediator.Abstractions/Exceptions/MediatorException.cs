namespace AnyKit.Mediator;

/// <summary>
/// Base exception type for all the exceptions that might occur in the AnyKit.Mediator packages.
/// </summary>
public abstract class MediatorException : ApplicationException
{
    protected MediatorException(string message) : base(message)
    {
    }

    protected MediatorException(string message, Exception innerException) : base(message, innerException)
    {
    }
}