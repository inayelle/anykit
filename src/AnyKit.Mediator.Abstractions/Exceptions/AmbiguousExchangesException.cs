namespace AnyKit.Mediator;

/// <summary>
/// Exception thrown when multiple exchanges can handle sent message, creating ambiguity.
/// </summary>
public sealed class AmbiguousExchangesException : MediatorException
{
    public Type MessageType { get; }
    public Type ResultType { get; }

    public AmbiguousExchangesException(Type messageType, Type resultType)
        : base(String.Format(SR.AmbiguousExchanges, messageType.FullName, resultType.FullName))
    {
        MessageType = messageType;
        ResultType = resultType;
    }
}