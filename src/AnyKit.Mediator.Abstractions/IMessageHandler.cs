namespace AnyKit.Mediator;

/// <summary>
/// Defines a handler for processing a specific type of message and returning a result.
/// </summary>
/// <typeparam name="TMessage">The type of the message being handled.</typeparam>
/// <typeparam name="TResult">The type of the result expected from the handler.</typeparam>
public interface IMessageHandler<in TMessage, TResult>
{
    /// <summary>
    /// Handles the processing of a message within a given exchange context.
    /// </summary>
    /// <param name="context">The context of the exchange containing the message.</param>
    /// <returns>A task that represents the asynchronous handling operation, containing the result from processing the message.</returns>
    Task<TResult> Handle(IExchangeContext<TMessage> context);
}

/// <summary>
/// Defines a handler for processing a specific type of message without returning a result.
/// It's a shorthand for the <see cref="IMessageHandler{TMessage,TResult}"/>, where TResult is <see cref="AnyKit.Mediator.None"/>.
/// </summary>
/// <typeparam name="TMessage">The type of the message being handled.</typeparam>
public interface IMessageHandler<in TMessage> : IMessageHandler<TMessage, None>
{
    /// <summary>
    /// Handles the processing of a message within a given exchange context.
    /// </summary>
    /// <param name="context">The context of the exchange containing the message.</param>
    /// <returns>A task that represents the asynchronous handling operation.</returns>
    new Task Handle(IExchangeContext<TMessage> context);

    /// <inheritdoc/>
    async Task<None> IMessageHandler<TMessage, None>.Handle(IExchangeContext<TMessage> context)
    {
        await Handle(context).ConfigureAwait(false);

        return new None();
    }
}