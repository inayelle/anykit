using AnyKit.Pipelines;

namespace AnyKit.Mediator;

/// <summary>
/// Builds a pipeline for processing messages in an exchange.
/// </summary>
/// <typeparam name="TMessage">The type of the message being processed.</typeparam>
/// <typeparam name="TResult">The type of the result produced by processing the message.</typeparam>
public interface IExchangePipelineBuilder<TMessage, TResult>
    : IAsyncPipelineBuilder<IExchangeContext<TMessage>, TResult>
{
    /// <summary>
    /// Adds a pipe instance to the pipeline.
    /// </summary>
    /// <param name="pipe">The pipe to be added.</param>
    void UsePipe(IExchangePipe pipe);

    /// <summary>
    /// Adds a generic pipe instance to the pipeline.
    /// </summary>
    /// <param name="pipe">The generic pipe to be added.</param>
    void UseGenericPipe(IExchangePipe<TMessage, TResult> pipe);

    /// <summary>
    /// Adds a pipe of type <typeparamref name="TPipe"/> to the pipeline.
    /// The pipe will be activated through <see cref="IServiceProvider"/> from the current <see cref="IExchangeContext{TMessage}"/>.
    /// </summary>
    /// <typeparam name="TPipe">The type of the pipe to be added.</typeparam>
    void UsePipe<TPipe>()
        where TPipe : class, IExchangePipe;

    /// <summary>
    /// Adds a generic pipe of type <typeparamref name="TPipe"/> to the pipeline.
    /// The pipe will be activated through <see cref="IServiceProvider"/> from the current <see cref="IExchangeContext{TMessage}"/>.
    /// </summary>
    /// <typeparam name="TPipe">The type of the generic pipe to be added.</typeparam>
    void UseGenericPipe<TPipe>()
        where TPipe : class, IExchangePipe<TMessage, TResult>;
}