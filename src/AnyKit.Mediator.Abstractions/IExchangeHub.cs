namespace AnyKit.Mediator;

/// <summary>
/// Manages sending and publishing messages through exchanges.
/// </summary>
/// <remarks>
/// Typically <see cref="ISender"/> and <see cref="IPublisher"/>, and thus <see cref="IMediator"/> push all the incoming messages to the hub.
/// </remarks>
public interface IExchangeHub
{
    /// <summary>
    /// Sends a message and returns a result produced by the corresponding exchange.
    /// </summary>
    /// <typeparam name="TMessage">The type of the message being sent.</typeparam>
    /// <typeparam name="TResult">The type of the result expected from the send operation.</typeparam>
    /// <param name="context">The context containing the message to be sent.</param>
    /// <returns>A task representing the asynchronous operation, containing the result of the send operation.</returns>
    /// <exception cref="AmbiguousExchangesException">Thrown when multiple exchanges can handle sent message, creating ambiguity.</exception>
    /// <exception cref="MissingExchangeException">Thrown when an expected exchange is missing for the given message and result types.</exception>
    /// <exception cref="Exception">Other exception types specific to the concrete hub implementation.</exception>
    Task<TResult> Send<TMessage, TResult>(MessageContext<TMessage> context);

    /// <summary>
    /// Publishes a message without expecting a result based on the message context.
    /// </summary>
    /// <typeparam name="TMessage">The type of the message being published.</typeparam>
    /// <param name="context">The context containing the message to be published.</param>
    /// <returns>A task representing the asynchronous publish operation.</returns>
    /// <exception cref="Exception">Other exception types specific to the concrete hub implementation.</exception>
    Task Publish<TMessage>(MessageContext<TMessage> context);
}