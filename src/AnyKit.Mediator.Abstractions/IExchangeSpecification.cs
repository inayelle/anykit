namespace AnyKit.Mediator;

/// <summary>
/// Configures an exchange pipeline.
/// </summary>
public interface IExchangeSpecification
{
    /// <summary>
    /// Applies the exchange specification to the given pipeline builder.
    /// </summary>
    /// <typeparam name="TMessage">The type of the message to be processed in the exchange.</typeparam>
    /// <typeparam name="TResult">The type of the result produced by the exchange.</typeparam>
    /// <param name="builder">The pipeline builder to which the specification will be applied.</param>
    void Apply<TMessage, TResult>(IExchangePipelineBuilder<TMessage, TResult> builder);
}