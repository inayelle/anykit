namespace AnyKit.Mediator;

/// <summary>
/// Encapsulates the context of a message, including the message itself and additional metadata.
/// </summary>
/// <typeparam name="TMessage">The type of the message being processed.</typeparam>
public readonly struct MessageContext<TMessage>
{
    /// <summary>
    /// The message being processed in the exchange.
    /// </summary>
    public TMessage Message { get; }

    /// <summary>
    /// The current service provider for resolving dependencies within the exchange.
    /// </summary>
    public CorrelationId CorrelationId { get; }

    /// <summary>
    /// The unique correlation ID used to correlate related messages or operations.
    /// </summary>
    public CancellationToken CancellationToken { get; }

    public MessageContext(TMessage message, CorrelationId correlationId, CancellationToken cancellationToken)
    {
        Message = message;
        CorrelationId = correlationId;
        CancellationToken = cancellationToken;
    }
}