namespace AnyKit.Mediator;

/// <summary>
/// Provides context for an exchange, including the message being processed and additional metadata.
/// Allows message sending and publication without need to additionally resolve <see cref="IMediator"/>, <see cref="ISender"/>, or <see cref="IPublisher"/>.
/// </summary>
/// <typeparam name="TMessage">The type of the message being processed.</typeparam>
public interface IExchangeContext<out TMessage> : ISender, IPublisher
{
    /// <summary>
    /// The message being processed in the exchange.
    /// </summary>
    TMessage Message { get; }

    /// <summary>
    /// The current service provider for resolving dependencies within the exchange.
    /// </summary>
    IServiceProvider ServiceProvider { get; }

    /// <summary>
    /// The unique correlation ID used to correlate related messages or operations.
    /// </summary>
    CorrelationId CorrelationId { get; }

    /// <summary>
    /// The cancellation token that can be used to cancel the exchange operation.
    /// </summary>
    CancellationToken CancellationToken { get; }
}