namespace AnyKit.Mediator;

/// <summary>
/// Provides a way to create a proxy for <see cref="IExchangeContext{TMessage}"/> and override its behavior.
/// </summary>
/// <typeparam name="TMessage">The type of the message being processed.</typeparam>
public abstract class ExchangeContextProxy<TMessage> : IExchangeContext<TMessage>
{
    private readonly IExchangeContext<TMessage> _context;

    /// <inheritdoc/>
    public virtual TMessage Message => _context.Message;

    /// <inheritdoc/>
    public virtual IServiceProvider ServiceProvider => _context.ServiceProvider;

    /// <inheritdoc/>
    public virtual CorrelationId CorrelationId => _context.CorrelationId;

    /// <inheritdoc/>
    public virtual CancellationToken CancellationToken => _context.CancellationToken;

    protected ExchangeContextProxy(IExchangeContext<TMessage> context)
    {
        _context = context;
    }

    /// <inheritdoc/>
    public virtual Task<TResult> Send<TOtherMessage, TResult>(
        TOtherMessage message,
        CancellationToken cancellationToken = default
    )
    {
        return _context.Send<TOtherMessage, TResult>(message, cancellationToken);
    }

    /// <inheritdoc/>
    public Task Send<TOtherMessage>(
        TOtherMessage message,
        CancellationToken cancellationToken = default
    )
    {
        return _context.Send(message, cancellationToken);
    }

    /// <inheritdoc/>
    public virtual Task Publish<TOtherMessage>(
        TOtherMessage message,
        CancellationToken cancellationToken = default
    )
    {
        return _context.Publish(message, cancellationToken);
    }
}