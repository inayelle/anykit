namespace AnyKit.Mediator;

/// <summary>
/// A gateway for publishing messages to the appropriate exchange(s).
/// </summary>
public interface IPublisher
{
    /// <summary>
    /// Publishes a message to the appropriate exchange(s).
    /// </summary>
    /// <typeparam name="TMessage">The type of the message being published.</typeparam>
    /// <param name="message">The message to be published.</param>
    /// <param name="cancellationToken">A token to cancel the operation.</param>
    /// <returns>A task that represents the asynchronous publish operation.</returns>
    Task Publish<TMessage>(TMessage message, CancellationToken cancellationToken = default);
}