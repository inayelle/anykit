namespace AnyKit.Mediator;

/// <summary>
/// An exchange that processes a message and returns a result.
/// </summary>
/// <typeparam name="TMessage">The type of the message being processed.</typeparam>
/// <typeparam name="TResult">The type of the result produced by processing the message.</typeparam>
public interface IExchange<in TMessage, TResult>
{
    /// <summary>
    /// Executes the underlying exchange pipeline using the provided context.
    /// </summary>
    /// <param name="context">The context of the exchange containing the message.</param>
    /// <returns>A task representing the asynchronous execution operation, containing the result of processing the message.</returns>
    Task<TResult> Execute(IExchangeContext<TMessage> context);
}