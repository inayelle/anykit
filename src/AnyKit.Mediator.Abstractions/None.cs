namespace AnyKit.Mediator;

/// <summary>
/// An empty type for exchanges, pipelines, and handlers that do not return any result.
/// </summary>
public readonly struct None
{
}