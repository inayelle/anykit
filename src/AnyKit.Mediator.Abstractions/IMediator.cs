namespace AnyKit.Mediator;

/// <summary>
/// A gateway for sending and publishing messages through exchanges.
/// </summary>
/// <remarks>
/// This interface combines the functionality of <see cref="ISender"/> and <see cref="IPublisher"/>.
/// </remarks>
public interface IMediator : ISender, IPublisher
{
}