namespace AnyKit.Mediator;

/// <summary>
/// Builds an exchange for arbitrary message and result types by applying specifications.
/// </summary>
public interface IExchangeBuilder
{
    /// <summary>
    /// Adds a specification to the exchange builder.
    /// </summary>
    /// <param name="specification">The specification to apply to the exchange.</param>
    void AddExchangeSpecification(IExchangeSpecification specification);
}

/// <summary>
/// Builds an exchange for a specific message and result type.
/// </summary>
/// <typeparam name="TMessage">The type of the message to be processed by the exchange.</typeparam>
/// <typeparam name="TResult">The type of the result produced by the exchange.</typeparam>
public interface IExchangeBuilder<TMessage, TResult> : IExchangeBuilder
{
}