namespace AnyKit.Mediator;

/// <summary>
/// A gateway for sending messages to the appropriate exchange.
/// </summary>
public interface ISender
{
    /// <summary>
    /// Sends a message to the appropriate exchange and returns a result.
    /// </summary>
    /// <typeparam name="TMessage">The type of the message being sent.</typeparam>
    /// <typeparam name="TResult">The type of the result expected from the handler.</typeparam>
    /// <param name="message">The message to be sent.</param>
    /// <param name="cancellationToken">A token to cancel the operation.</param>
    /// <returns>A task that represents the asynchronous send operation, containing the result from the handler.</returns>
    Task<TResult> Send<TMessage, TResult>(TMessage message, CancellationToken cancellationToken = default);

    /// <summary>
    /// Sends a message to the appropriate exchange.
    /// </summary>
    /// <typeparam name="TMessage">The type of the message being sent.</typeparam>
    /// <param name="message">The message to be sent.</param>
    /// <param name="cancellationToken">A token to cancel the operation.</param>
    /// <returns>A task that represents the asynchronous send operation.</returns>
    Task Send<TMessage>(TMessage message, CancellationToken cancellationToken = default);
}