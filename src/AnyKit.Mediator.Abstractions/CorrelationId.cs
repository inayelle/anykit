namespace AnyKit.Mediator;

/// <summary>
/// Represents a unique identifier for correlating related messages or operations.
/// </summary>
public readonly struct CorrelationId : IEquatable<CorrelationId>, IComparable<CorrelationId>
{
    private static long _source = TimeProvider.System.GetUtcNow().Ticks;

    private readonly long _value;

    private CorrelationId(long value)
    {
        _value = value;
    }

    public static CorrelationId Create()
    {
        var value = Interlocked.Increment(ref _source);

        return new CorrelationId(value);
    }

    public bool Equals(CorrelationId other)
    {
        return _value == other._value;
    }

    public override bool Equals(object obj)
    {
        return obj is CorrelationId conversationId && Equals(conversationId);
    }

    public override int GetHashCode()
    {
        return _value.GetHashCode();
    }

    public override string ToString()
    {
        return _value.ToString("X");
    }

    public static bool operator ==(CorrelationId left, CorrelationId right)
    {
        return left.Equals(right);
    }

    public static bool operator !=(CorrelationId left, CorrelationId right)
    {
        return !left.Equals(right);
    }

    public static implicit operator long(CorrelationId exchangeId)
    {
        return exchangeId._value;
    }

    public int CompareTo(CorrelationId other)
    {
        return _value.CompareTo(other._value);
    }
}