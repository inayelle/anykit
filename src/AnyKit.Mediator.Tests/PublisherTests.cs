using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using NSubstitute;

namespace AnyKit.Mediator;

[TestFixture]
[TestOf(typeof(IPublisher))]
internal sealed class PublisherTests
{
    private const int InputValue = 322;

    private ServiceProvider _serviceProvider;
    private IPublisher _publisher;
    private IExchangePipe _pipeInstance0;
    private IExchangePipe _pipeInstance1;
    private IExchangePipe _pipeInstance2;
    private TagTracker _tagTracker;

    [Test]
    [TestCaseSource(nameof(EventTestCaseSource))]
    public async Task Publisher_ShouldPublish_ToAllPresentExchangesForEvent<TMessage>(
        TMessage message,
        string[] expectedTags
    )
    {
        // Act
        await _publisher.Publish(message);

        // Assert
        Assert.That(
            () => _tagTracker.Tags,
            Is.EquivalentTo(expectedTags).After(30).Seconds.PollEvery(100).MilliSeconds
        );

        Received.InOrder(() =>
            {
                _pipeInstance0.Received(3);
                _pipeInstance1.Received(3);
                _pipeInstance2.Received(3);
            }
        );
    }

    [SetUp]
    public void SetUp()
    {
        _pipeInstance0 = ExchangePipeSubstitute.InvokesNext();
        _pipeInstance1 = ExchangePipeSubstitute.InvokesNext();
        _pipeInstance2 = ExchangePipeSubstitute.InvokesNext();

        var services = new ServiceCollection();

        services.AddSingleton<TagTracker>();

        services.AddMediator(mediator =>
            {
                mediator.AddMessageHandlers(Assembly.GetExecutingAssembly());
                mediator.UsePipe(_pipeInstance0);
                mediator.UsePipe(_pipeInstance1);
                mediator.UsePipe(_pipeInstance2);
            }
        );

        _serviceProvider = services.BuildServiceProvider();
        _publisher = _serviceProvider.GetRequiredService<IPublisher>();
        _tagTracker = _serviceProvider.GetRequiredService<TagTracker>();
    }

    [TearDown]
    public void TearDown()
    {
        _serviceProvider.Dispose();
    }

    private static IEnumerable<object[]> EventTestCaseSource()
    {
        yield return
        [
            new Messages.Class.Event
            {
                Value = InputValue,
            },
            new[]
            {
                typeof(Handlers.Class.EventHandler0).FullName,
                typeof(Handlers.Class.EventHandler1).FullName,
                typeof(Handlers.Class.EventHandler2).FullName,
            }
        ];

        yield return
        [
            new Messages.Struct.Event
            {
                Value = InputValue,
            },
            new[]
            {
                typeof(Handlers.Struct.EventHandler0).FullName,
                typeof(Handlers.Struct.EventHandler1).FullName,
                typeof(Handlers.Struct.EventHandler2).FullName,
            }
        ];
    }
}