namespace AnyKit.Mediator;

internal sealed class TagTracker
{
    private readonly HashSet<string> _tags = [];

    public IReadOnlyCollection<string> Tags => _tags;

    public void Emit(string tag)
    {
        _tags.Add(tag);
    }

    public bool ContainsTag(string tag)
    {
        return _tags.Contains(tag);
    }
}