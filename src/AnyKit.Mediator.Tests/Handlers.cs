namespace AnyKit.Mediator;

internal static class Handlers
{
    public static class Class
    {
        public class CommandHandler : IMessageHandler<Messages.Class.Command>
        {
            private readonly TagTracker _tagTracker;

            public CommandHandler(TagTracker tagTracker)
            {
                _tagTracker = tagTracker;
            }

            public Task Handle(IExchangeContext<Messages.Class.Command> context)
            {
                _tagTracker.Emit(typeof(CommandHandler).FullName);

                return Task.CompletedTask;
            }
        }

        public class QueryHandler : IMessageHandler<Messages.Class.Query, Messages.Class.Result>
        {
            private readonly TagTracker _tagTracker;

            public QueryHandler(TagTracker tagTracker)
            {
                _tagTracker = tagTracker;
            }

            public Task<Messages.Class.Result> Handle(IExchangeContext<Messages.Class.Query> context)
            {
                _tagTracker.Emit(typeof(QueryHandler).FullName);

                return Task.FromResult(new Messages.Class.Result
                    {
                        Value = context.Message.Value,
                    }
                );
            }
        }

        public class EventHandler0 : IMessageHandler<Messages.Class.Event>
        {
            private readonly TagTracker _tagTracker;

            public EventHandler0(TagTracker tagTracker)
            {
                _tagTracker = tagTracker;
            }

            public Task Handle(IExchangeContext<Messages.Class.Event> context)
            {
                _tagTracker.Emit(typeof(EventHandler0).FullName);

                return Task.CompletedTask;
            }
        }

        public class EventHandler1 : IMessageHandler<Messages.Class.Event>
        {
            private readonly TagTracker _tagTracker;

            public EventHandler1(TagTracker tagTracker)
            {
                _tagTracker = tagTracker;
            }

            public Task Handle(IExchangeContext<Messages.Class.Event> context)
            {
                _tagTracker.Emit(typeof(EventHandler1).FullName);

                return Task.CompletedTask;
            }
        }

        public class EventHandler2 : IMessageHandler<Messages.Class.Event>
        {
            private readonly TagTracker _tagTracker;

            public EventHandler2(TagTracker tagTracker)
            {
                _tagTracker = tagTracker;
            }

            public Task Handle(IExchangeContext<Messages.Class.Event> context)
            {
                _tagTracker.Emit(typeof(EventHandler2).FullName);

                return Task.CompletedTask;
            }
        }

        public class MultipleHandlers0 : IMessageHandler<Messages.Class.MultipleHandlers>
        {
            public Task Handle(IExchangeContext<Messages.Class.MultipleHandlers> context)
            {
                return Task.CompletedTask;
            }
        }

        public class MultipleHandlers1 : IMessageHandler<Messages.Class.MultipleHandlers>
        {
            public Task Handle(IExchangeContext<Messages.Class.MultipleHandlers> context)
            {
                return Task.CompletedTask;
            }
        }
    }

    public static class Struct
    {
        public class CommandHandler : IMessageHandler<Messages.Struct.Command>
        {
            private readonly TagTracker _tagTracker;

            public CommandHandler(TagTracker tagTracker)
            {
                _tagTracker = tagTracker;
            }

            public Task Handle(IExchangeContext<Messages.Struct.Command> context)
            {
                _tagTracker.Emit(typeof(CommandHandler).FullName);

                return Task.CompletedTask;
            }
        }

        public class QueryHandler : IMessageHandler<Messages.Struct.Query, Messages.Struct.Result>
        {
            private readonly TagTracker _tagTracker;

            public QueryHandler(TagTracker tagTracker)
            {
                _tagTracker = tagTracker;
            }

            public Task<Messages.Struct.Result> Handle(IExchangeContext<Messages.Struct.Query> context)
            {
                _tagTracker.Emit(typeof(QueryHandler).FullName);

                return Task.FromResult(new Messages.Struct.Result
                    {
                        Value = context.Message.Value,
                    }
                );
            }
        }

        public class EventHandler0 : IMessageHandler<Messages.Struct.Event>
        {
            private readonly TagTracker _tagTracker;

            public EventHandler0(TagTracker tagTracker)
            {
                _tagTracker = tagTracker;
            }

            public Task Handle(IExchangeContext<Messages.Struct.Event> context)
            {
                _tagTracker.Emit(typeof(EventHandler0).FullName);

                return Task.CompletedTask;
            }
        }

        public class EventHandler1 : IMessageHandler<Messages.Struct.Event>
        {
            private readonly TagTracker _tagTracker;

            public EventHandler1(TagTracker tagTracker)
            {
                _tagTracker = tagTracker;
            }

            public Task Handle(IExchangeContext<Messages.Struct.Event> context)
            {
                _tagTracker.Emit(typeof(EventHandler1).FullName);

                return Task.CompletedTask;
            }
        }

        public class EventHandler2 : IMessageHandler<Messages.Struct.Event>
        {
            private readonly TagTracker _tagTracker;

            public EventHandler2(TagTracker tagTracker)
            {
                _tagTracker = tagTracker;
            }

            public Task Handle(IExchangeContext<Messages.Struct.Event> context)
            {
                _tagTracker.Emit(typeof(EventHandler2).FullName);

                return Task.CompletedTask;
            }
        }

        public class MultipleHandlers0 : IMessageHandler<Messages.Struct.MultipleHandlers>
        {
            public Task Handle(IExchangeContext<Messages.Struct.MultipleHandlers> context)
            {
                return Task.CompletedTask;
            }
        }

        public class MultipleHandlers1 : IMessageHandler<Messages.Struct.MultipleHandlers>
        {
            public Task Handle(IExchangeContext<Messages.Struct.MultipleHandlers> context)
            {
                return Task.CompletedTask;
            }
        }
    }
}