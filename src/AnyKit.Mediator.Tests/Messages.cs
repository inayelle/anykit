namespace AnyKit.Mediator;

internal static class Messages
{
    public static class Class
    {
        public sealed class Command
        {
            public required int Value { get; init; }
        }

        public sealed class Query
        {
            public required int Value { get; init; }
        }

        public sealed class Event
        {
            public required int Value { get; init; }
        }

        public sealed class NoHandler
        {
            public required int Value { get; init; }
        }

        public sealed class MultipleHandlers
        {
            public required int Value { get; init; }
        }

        public sealed class Result : IEquatable<Result>
        {
            public required int Value { get; init; }

            public bool Equals(Result other)
            {
                if (other is null)
                {
                    return false;
                }

                if (ReferenceEquals(this, other))
                {
                    return true;
                }

                return Value == other.Value;
            }

            public override bool Equals(object obj)
            {
                return ReferenceEquals(this, obj) || obj is Result other && Equals(other);
            }

            public override int GetHashCode()
            {
                return Value;
            }
        }
    }

    public static class Struct
    {
        public readonly struct Command
        {
            public required int Value { get; init; }
        }

        public readonly struct Query
        {
            public required int Value { get; init; }
        }

        public readonly struct Event
        {
            public required int Value { get; init; }
        }

        public readonly struct NoHandler
        {
            public required int Value { get; init; }
        }

        public readonly struct MultipleHandlers
        {
            public required int Value { get; init; }
        }

        public readonly struct Result : IEquatable<Result>
        {
            public required int Value { get; init; }

            public bool Equals(Result other)
            {
                return Value == other.Value;
            }

            public override bool Equals(object obj)
            {
                return obj is Result other && Equals(other);
            }

            public override int GetHashCode()
            {
                return Value;
            }
        }
    }
}