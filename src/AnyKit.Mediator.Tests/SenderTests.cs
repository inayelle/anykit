using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using NSubstitute;

namespace AnyKit.Mediator;

[TestFixture]
[TestOf(typeof(ISender))]
internal sealed class SenderTests
{
    private const int InputValue = 322;

    private ServiceProvider _serviceProvider;
    private ISender _sender;
    private IExchangePipe _pipeInstance0;
    private IExchangePipe _pipeInstance1;
    private IExchangePipe _pipeInstance2;
    private TagTracker _tagTracker;

    [Test]
    [TestCaseSource(nameof(CommandTestCaseSource))]
    public async Task Send_ShouldReturnNone_WithPresentExchangeForCommand<TMessage>(
        TMessage message,
        string expectedTag
    )
    {
        // Act
        await _sender.Send(message);

        // Assert
        Assert.That(() => _tagTracker.ContainsTag(expectedTag), Is.True);

        Received.InOrder(() =>
            {
                _pipeInstance0.Received(1);
                _pipeInstance1.Received(1);
                _pipeInstance2.Received(1);
            }
        );
    }

    [Test]
    [TestCaseSource(nameof(QueryTestCaseSource))]
    public async Task Send_ShouldReturnResult_WithPresentExchangeForQuery<TMessage, TResult>(
        TMessage message,
        TResult expectedResult,
        string expectedTag
    )
    {
        // Act
        var actualResult = await _sender.Send<TMessage, TResult>(message);

        // Assert
        Assert.That(actualResult, Is.EqualTo(expectedResult));

        Assert.That(() => _tagTracker.ContainsTag(expectedTag), Is.True);

        Received.InOrder(() =>
            {
                _pipeInstance0.Received(1);
                _pipeInstance1.Received(1);
                _pipeInstance2.Received(1);
            }
        );
    }

    [Test]
    [TestCaseSource(nameof(NoExchangeTestCaseSource))]
    public void Send_ShouldThrowMissingExchangeException_WithoutPresentExchangeForCommand<TMessage>(
        TMessage message
    )
    {
        // Act & Assert
        Assert.ThrowsAsync<MissingExchangeException>(() => _sender.Send(message));

        _pipeInstance0.DidNotReceive();
        _pipeInstance1.DidNotReceive();
        _pipeInstance2.DidNotReceive();
    }

    [Test]
    [TestCaseSource(nameof(MultipleExchangesTestCaseSource))]
    public void Send_ShouldThrowAmbiguousExchangesException_WithPresentMultipleExchangesForCommand<TMessage>(
        TMessage message
    )
    {
        // Act & Assert
        Assert.ThrowsAsync<AmbiguousExchangesException>(() => _sender.Send(message));

        _pipeInstance0.DidNotReceive();
        _pipeInstance1.DidNotReceive();
        _pipeInstance2.DidNotReceive();
    }

    [SetUp]
    public void SetUp()
    {
        _pipeInstance0 = ExchangePipeSubstitute.InvokesNext();
        _pipeInstance1 = ExchangePipeSubstitute.InvokesNext();
        _pipeInstance2 = ExchangePipeSubstitute.InvokesNext();

        var services = new ServiceCollection();

        services.AddSingleton<TagTracker>();

        services.AddMediator(mediator =>
            {
                mediator.AddMessageHandlers(Assembly.GetExecutingAssembly());
                mediator.UsePipe(_pipeInstance0);
                mediator.UsePipe(_pipeInstance1);
                mediator.UsePipe(_pipeInstance2);
            }
        );

        _serviceProvider = services.BuildServiceProvider();
        _sender = _serviceProvider.GetRequiredService<ISender>();
        _tagTracker = _serviceProvider.GetRequiredService<TagTracker>();
    }

    [TearDown]
    public void TearDown()
    {
        _serviceProvider.Dispose();
    }

    private static IEnumerable<object[]> CommandTestCaseSource()
    {
        yield return
        [
            new Messages.Class.Command
            {
                Value = InputValue,
            },
            typeof(Handlers.Class.CommandHandler).FullName
        ];

        yield return
        [
            new Messages.Struct.Command
            {
                Value = InputValue,
            },
            typeof(Handlers.Struct.CommandHandler).FullName
        ];
    }

    private static IEnumerable<object[]> QueryTestCaseSource()
    {
        yield return
        [
            new Messages.Class.Query
            {
                Value = InputValue
            },
            new Messages.Class.Result
            {
                Value = InputValue
            },
            typeof(Handlers.Class.QueryHandler).FullName
        ];

        yield return
        [
            new Messages.Struct.Query
            {
                Value = InputValue
            },
            new Messages.Struct.Result
            {
                Value = InputValue
            },
            typeof(Handlers.Struct.QueryHandler).FullName
        ];
    }

    private static IEnumerable<object> NoExchangeTestCaseSource()
    {
        yield return new Messages.Class.NoHandler
        {
            Value = InputValue,
        };

        yield return new Messages.Struct.NoHandler
        {
            Value = InputValue,
        };
    }

    private static IEnumerable<object> MultipleExchangesTestCaseSource()
    {
        yield return new Messages.Class.MultipleHandlers
        {
            Value = InputValue,
        };

        yield return new Messages.Struct.MultipleHandlers
        {
            Value = InputValue,
        };
    }
}