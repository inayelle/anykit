using AnyKit.Pipelines;
using NSubstitute;

namespace AnyKit.Mediator;

public static class ExchangePipeSubstitute
{
    public static IExchangePipe InvokesNext()
    {
        return Substitute.ForPartsOf<InvokesNextPipe>();
    }

    public class InvokesNextPipe : IExchangePipe
    {
        public virtual Task<TResult> Invoke<TMessage, TResult>(
            IExchangeContext<TMessage> context,
            AsyncPipeline<IExchangeContext<TMessage>, TResult> next
        )
        {
            return next.Invoke(context);
        }
    }
}