namespace AnyKit.Pipelines;

internal static class AsyncPipelineDelegateFactory
{
    public static AsyncPipeline<TContext> Empty<TContext>()
    {
        return _ => Task.CompletedTask;
    }

    public static AsyncPipeline<TContext, TResult> Empty<TContext, TResult>()
    {
        return _ => Task.FromResult(default(TResult));
    }

    public static AsyncPipeline<TContext> AttachPipe<TContext>(
        AsyncPipeline<TContext> pipeline,
        AsyncPipe<TContext> pipe
    )
    {
        return context => pipe.Invoke(context, pipeline);
    }

    public static AsyncPipeline<TContext, TResult> AttachPipe<TContext, TResult>(
        AsyncPipeline<TContext, TResult> pipeline,
        AsyncPipe<TContext, TResult> pipe
    )
    {
        return context => pipe.Invoke(context, pipeline);
    }
}