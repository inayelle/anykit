namespace AnyKit.Pipelines;

internal static class PipelineDelegateFactory
{
    public static Pipeline<TContext> Empty<TContext>()
    {
        return _ => { };
    }

    public static Pipeline<TContext, TResult> Empty<TContext, TResult>()
    {
        return _ => default;
    }

    public static Pipeline<TContext> AttachPipe<TContext>(
        Pipeline<TContext> pipeline,
        Pipe<TContext> pipe
    )
    {
        return context => pipe.Invoke(context, pipeline);
    }

    public static Pipeline<TContext, TResult> AttachPipe<TContext, TResult>(
        Pipeline<TContext, TResult> pipeline,
        Pipe<TContext, TResult> pipe
    )
    {
        return context => pipe.Invoke(context, pipeline);
    }
}