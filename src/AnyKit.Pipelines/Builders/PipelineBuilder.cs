namespace AnyKit.Pipelines;

public class PipelineBuilder<TContext> : IPipelineBuilder<TContext>
{
    private readonly Stack<Pipe<TContext>> _pipes;

    public PipelineBuilder()
    {
        _pipes = new Stack<Pipe<TContext>>();
    }

    public PipelineBuilder(IEnumerable<Pipe<TContext>> pipes)
    {
        _pipes = new Stack<Pipe<TContext>>(pipes);
    }

    public virtual void UsePipe(Pipe<TContext> pipe)
    {
        ArgumentNullException.ThrowIfNull(pipe);

        _pipes.Push(pipe);
    }

    public virtual Pipeline<TContext> Build()
    {
        var pipeline = PipelineDelegateFactory.Empty<TContext>();

        return _pipes.Aggregate(pipeline, PipelineDelegateFactory.AttachPipe);
    }
}

public class PipelineBuilder<TContext, TResult> : IPipelineBuilder<TContext, TResult>
{
    private readonly Stack<Pipe<TContext, TResult>> _pipes;

    public PipelineBuilder()
    {
        _pipes = new Stack<Pipe<TContext, TResult>>();
    }

    public PipelineBuilder(IEnumerable<Pipe<TContext, TResult>> pipes)
    {
        _pipes = new Stack<Pipe<TContext, TResult>>(pipes);
    }

    public virtual void UsePipe(Pipe<TContext, TResult> pipe)
    {
        ArgumentNullException.ThrowIfNull(pipe);

        _pipes.Push(pipe);
    }

    public virtual Pipeline<TContext, TResult> Build()
    {
        var pipeline = PipelineDelegateFactory.Empty<TContext, TResult>();

        return _pipes.Aggregate(pipeline, PipelineDelegateFactory.AttachPipe);
    }
}