namespace AnyKit.Pipelines;

public class AsyncPipelineBuilder<TContext> : IAsyncPipelineBuilder<TContext>
{
    private readonly Stack<AsyncPipe<TContext>> _pipes;

    public AsyncPipelineBuilder()
    {
        _pipes = new Stack<AsyncPipe<TContext>>();
    }

    public AsyncPipelineBuilder(IEnumerable<AsyncPipe<TContext>> pipes)
    {
        _pipes = new Stack<AsyncPipe<TContext>>(pipes);
    }

    public virtual void UsePipe(AsyncPipe<TContext> pipe)
    {
        ArgumentNullException.ThrowIfNull(pipe);

        _pipes.Push(pipe);
    }

    public virtual AsyncPipeline<TContext> Build()
    {
        var pipeline = AsyncPipelineDelegateFactory.Empty<TContext>();

        return _pipes.Aggregate(pipeline, AsyncPipelineDelegateFactory.AttachPipe);
    }
}

public class AsyncPipelineBuilder<TContext, TResult> : IAsyncPipelineBuilder<TContext, TResult>
{
    private readonly Stack<AsyncPipe<TContext, TResult>> _pipes;

    public AsyncPipelineBuilder()
    {
        _pipes = new Stack<AsyncPipe<TContext, TResult>>();
    }

    public AsyncPipelineBuilder(IEnumerable<AsyncPipe<TContext, TResult>> pipes)
    {
        _pipes = new Stack<AsyncPipe<TContext, TResult>>(pipes);
    }

    public virtual void UsePipe(AsyncPipe<TContext, TResult> pipe)
    {
        ArgumentNullException.ThrowIfNull(pipe);

        _pipes.Push(pipe);
    }

    public virtual AsyncPipeline<TContext, TResult> Build()
    {
        var pipeline = AsyncPipelineDelegateFactory.Empty<TContext, TResult>();

        return _pipes.Aggregate(pipeline, AsyncPipelineDelegateFactory.AttachPipe);
    }
}