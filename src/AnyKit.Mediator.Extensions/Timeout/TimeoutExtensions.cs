namespace AnyKit.Mediator;

/// <summary>
/// Provides extension methods for configuring timeout in <see cref="IExchangeBuilder"/>.
/// </summary>
public static class TimeoutExtensions
{
    /// <summary>
    /// Configures the exchange builder to use a timeout for message processing.
    /// </summary>
    /// <param name="builder">The exchange builder to which the timeout configuration will be added.</param>
    /// <param name="timeout">The timeout duration to apply to the exchange operations.</param>
    public static void UseTimeout(this IExchangeBuilder builder, TimeSpan timeout)
    {
        ArgumentNullException.ThrowIfNull(builder);

        builder.AddExchangeSpecification(new TimeoutSpecification(timeout));
    }
}