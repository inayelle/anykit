namespace AnyKit.Mediator;

internal sealed class TimeoutSpecification : IExchangeSpecification
{
    private readonly TimeSpan _timeout;

    public TimeoutSpecification(TimeSpan timeout)
    {
        if (timeout <= TimeSpan.Zero)
        {
            throw new ArgumentException("Timeout must be greater than TimeSpan.Zero", nameof(timeout));
        }

        _timeout = timeout;
    }

    public void Apply<TMessage, TResult>(IExchangePipelineBuilder<TMessage, TResult> builder)
    {
        builder.UsePipe(new TimeoutPipe(_timeout));
    }
}