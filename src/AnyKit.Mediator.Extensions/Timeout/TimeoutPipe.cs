using AnyKit.Pipelines;

namespace AnyKit.Mediator;

internal sealed class TimeoutPipe : IExchangePipe
{
    private readonly TimeSpan _timeout;

    public TimeoutPipe(TimeSpan timeout)
    {
        _timeout = timeout;
    }

    public async Task<TResult> Invoke<TMessage, TResult>(
        IExchangeContext<TMessage> context,
        AsyncPipeline<IExchangeContext<TMessage>, TResult> next
    )
    {
        return await next(context).WaitAsync(_timeout).ConfigureAwait(false);
    }
}