namespace AnyKit.Mediator;

/// <summary>
/// Provides extension methods for configuring an outbox in <see cref="IExchangeBuilder"/>.
/// </summary>
public static class OutboxExtensions
{
    /// <summary>
    /// Configures the exchange builder to use an outbox in the exchange pipeline.
    /// </summary>
    /// <param name="builder">The exchange builder to which the outbox configuration will be added.</param>
    public static void UseOutbox(this IExchangeBuilder builder)
    {
        ArgumentNullException.ThrowIfNull(builder);

        builder.AddExchangeSpecification(new OutboxSpecification());
    }
}