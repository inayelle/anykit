namespace AnyKit.Mediator;

internal sealed class OutboxSpecification : IExchangeSpecification
{
    public void Apply<TMessage, TResult>(IExchangePipelineBuilder<TMessage, TResult> builder)
    {
        builder.UsePipe(new OutboxPipe());
    }
}