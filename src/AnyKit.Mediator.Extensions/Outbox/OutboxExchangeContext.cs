namespace AnyKit.Mediator;

internal sealed class OutboxExchangeContext<TMessage> : ExchangeContextProxy<TMessage>
{
    private readonly List<Func<Task>> _preservedPublications = [];

    public OutboxExchangeContext(IExchangeContext<TMessage> context) : base(context)
    {
    }

    public override Task Publish<TOtherMessage>(
        TOtherMessage message,
        CancellationToken cancellationToken = default
    )
    {
        _preservedPublications.Add(() => base.Publish(message, cancellationToken));

        return Task.CompletedTask;
    }

    public Task Publish()
    {
        return _preservedPublications switch
        {
            [] => Task.CompletedTask,
            [var publication] => publication.Invoke(),
            _ => Task.WhenAll(_preservedPublications.Select(Task.Run)),
        };
    }
}