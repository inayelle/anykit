using AnyKit.Pipelines;

namespace AnyKit.Mediator;

internal sealed class OutboxPipe : IExchangePipe
{
    public async Task<TResult> Invoke<TMessage, TResult>(
        IExchangeContext<TMessage> context,
        AsyncPipeline<IExchangeContext<TMessage>, TResult> next
    )
    {
        var outboxContext = new OutboxExchangeContext<TMessage>(context);

        var result = await next(outboxContext).ConfigureAwait(false);

        await outboxContext.Publish().ConfigureAwait(false);

        return result;
    }
}