using Microsoft.Extensions.DependencyInjection;

namespace AnyKit.Mediator;

internal interface IExchangeDescriptor
{
    ServiceDescriptor MakeServiceDescriptor(ServiceLifetime lifetime);
}