using Microsoft.Extensions.DependencyInjection;

namespace AnyKit.Mediator;

internal sealed class ExchangeDescriptor<THandler, TMessage, TResult> : IExchangeDescriptor
    where THandler : class, IMessageHandler<TMessage, TResult>
{
    public ServiceDescriptor MakeServiceDescriptor(ServiceLifetime lifetime)
    {
        var exchangeType = typeof(IExchange<TMessage, TResult>);

        return new ServiceDescriptor(exchangeType, Factory, lifetime);
    }

    private static IExchange<TMessage, TResult> Factory(IServiceProvider serviceProvider)
    {
        var exchangeBuilder = serviceProvider.CreateInstance<ExchangeBuilder<THandler, TMessage, TResult>>();

        var specifications = serviceProvider.GetServices<IExchangeSpecification>();

        foreach (var specification in specifications)
        {
            exchangeBuilder.AddExchangeSpecification(specification);
        }

        var configurator = serviceProvider.GetService<IExchangeConfigurator<THandler, TMessage, TResult>>();

        configurator?.Configure(exchangeBuilder);

        return exchangeBuilder.Build();
    }
}