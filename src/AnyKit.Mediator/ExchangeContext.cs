namespace AnyKit.Mediator;

/// <inheritdoc/>
internal sealed class ExchangeContext<TMessage> : IExchangeContext<TMessage>
{
    private readonly IExchangeHub _exchangeHub;

    /// <inheritdoc/>
    public TMessage Message { get; }

    /// <inheritdoc/>
    public IServiceProvider ServiceProvider { get; }

    /// <inheritdoc/>
    public CorrelationId CorrelationId { get; }

    /// <inheritdoc/>
    public CancellationToken CancellationToken { get; }

    public ExchangeContext(
        IExchangeHub exchangeHub,
        TMessage message,
        IServiceProvider serviceProvider,
        CorrelationId correlationId,
        CancellationToken cancellationToken
    )
    {
        _exchangeHub = exchangeHub;

        Message = message;
        ServiceProvider = serviceProvider;
        CorrelationId = correlationId;
        CancellationToken = cancellationToken;
    }

    /// <inheritdoc/>
    public Task<TResult> Send<TOtherMessage, TResult>(
        TOtherMessage message,
        CancellationToken cancellationToken = default
    )
    {
        var context = new MessageContext<TOtherMessage>(message, CorrelationId, cancellationToken);

        return _exchangeHub.Send<TOtherMessage, TResult>(context);
    }

    /// <inheritdoc/>
    public Task Send<TOtherMessage>(
        TOtherMessage message,
        CancellationToken cancellationToken = default
    )
    {
        var context = new MessageContext<TOtherMessage>(message, CorrelationId, cancellationToken);

        return _exchangeHub.Send<TOtherMessage, None>(context);
    }

    /// <inheritdoc/>
    public Task Publish<TOtherMessage>(
        TOtherMessage message,
        CancellationToken cancellationToken = default
    )
    {
        var context = new MessageContext<TOtherMessage>(message, CorrelationId, cancellationToken);

        return _exchangeHub.Publish(context);
    }
}