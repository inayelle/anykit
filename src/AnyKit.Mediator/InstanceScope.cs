namespace AnyKit.Mediator;

internal readonly struct InstanceScope<T> : IAsyncDisposable
{
    public T Value { get; }

    public InstanceScope(T value)
    {
        Value = value;
    }

    public ValueTask DisposeAsync()
    {
        switch (Value)
        {
            case IAsyncDisposable asyncDisposable:
                return asyncDisposable.DisposeAsync();
            case IDisposable disposable:
                disposable.Dispose();
                return ValueTask.CompletedTask;
            default:
                return ValueTask.CompletedTask;
        }
    }
}