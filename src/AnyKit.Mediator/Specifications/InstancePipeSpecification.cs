namespace AnyKit.Mediator;

/// <inheritdoc/>
internal sealed class InstancePipeSpecification : IExchangeSpecification
{
    private readonly IExchangePipe _pipe;

    public InstancePipeSpecification(IExchangePipe pipe)
    {
        _pipe = pipe;
    }

    /// <inheritdoc/>
    public void Apply<TMessage, TResult>(IExchangePipelineBuilder<TMessage, TResult> builder)
    {
        builder.UsePipe(_pipe);
    }
}