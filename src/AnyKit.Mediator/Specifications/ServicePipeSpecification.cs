namespace AnyKit.Mediator;

/// <inheritdoc/>
internal sealed class ServicePipeSpecification<TPipe> : IExchangeSpecification
    where TPipe : class, IExchangePipe
{
    /// <inheritdoc/>
    public void Apply<TMessage, TResult>(IExchangePipelineBuilder<TMessage, TResult> builder)
    {
        builder.UsePipe<TPipe>();
    }
}