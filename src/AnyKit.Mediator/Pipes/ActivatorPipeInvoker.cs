using AnyKit.Pipelines;

namespace AnyKit.Mediator;

internal static class ActivatorPipeInvoker
{
    public static async Task<TResult> Invoke<TPipe, TMessage, TResult>(
        IExchangeContext<TMessage> context,
        AsyncPipeline<IExchangeContext<TMessage>, TResult> next
    ) where TPipe : class, IExchangePipe
    {
        var instanceScope = context.ServiceProvider.CreateInstanceScopeWithFactoryCache<TPipe>();
        await using var _ = instanceScope.ConfigureAwait(false);

        return await instanceScope.Value.Invoke(context, next).ConfigureAwait(false);
    }

    public static async Task<TResult> InvokeGeneric<TPipe, TMessage, TResult>(
        IExchangeContext<TMessage> context,
        AsyncPipeline<IExchangeContext<TMessage>, TResult> next
    ) where TPipe : class, IExchangePipe<TMessage, TResult>
    {
        var pipe = context.ServiceProvider.CreateInstanceScopeWithFactoryCache<TPipe>();
        await using var _ = pipe.ConfigureAwait(false);

        return await pipe.Value.Invoke(context, next).ConfigureAwait(false);
    }
}