using AnyKit.Pipelines;

namespace AnyKit.Mediator;

internal static class MessageHandlerInvoker
{
    public static async Task<TResult> Invoke<THandler, TMessage, TResult>(
        IExchangeContext<TMessage> context,
        AsyncPipeline<IExchangeContext<TMessage>, TResult> next
    ) where THandler : class, IMessageHandler<TMessage, TResult>
    {
        var instanceScope = context.ServiceProvider.CreateInstanceScopeWithFactoryCache<THandler>();
        await using var _ = instanceScope.ConfigureAwait(false);

        return await instanceScope.Value.Handle(context).ConfigureAwait(false);
    }
}