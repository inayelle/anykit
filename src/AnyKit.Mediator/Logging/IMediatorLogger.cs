using Microsoft.Extensions.Logging;

namespace AnyKit.Mediator;

internal interface IMediatorLogger : ILogger
{
}