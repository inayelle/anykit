using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace AnyKit.Mediator;

internal static class MediatorLoggerFactory
{
    private const string MediatorLoggerCategory = "AnyKit.Mediator";

    public static IMediatorLogger Create(IServiceProvider serviceProvider)
    {
        var loggerFactory = serviceProvider.GetService<ILoggerFactory>();

        var logger = loggerFactory?.CreateLogger(MediatorLoggerCategory);

        return new MediatorLogger(logger);
    }
}