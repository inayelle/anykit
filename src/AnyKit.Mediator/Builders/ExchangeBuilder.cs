using AnyKit.Pipelines;
using Microsoft.Extensions.Logging;

namespace AnyKit.Mediator;

/// <inheritdoc/>
internal sealed class ExchangeBuilder<THandler, TMessage, TResult> : IExchangeBuilder<TMessage, TResult>
    where THandler : class, IMessageHandler<TMessage, TResult>
{
    private readonly List<IExchangeSpecification> _specifications;
    private readonly IMediatorLogger _logger;

    public ExchangeBuilder(
        IMediatorLogger logger
    )
    {
        _specifications = [];
        _logger = logger;
    }

    /// <inheritdoc/>
    public void AddExchangeSpecification(IExchangeSpecification specification)
    {
        ArgumentNullException.ThrowIfNull(specification);

        _specifications.Add(specification);
    }

    public IExchange<TMessage, TResult> Build()
    {
        var pipeline = BuildPipeline();

        var exchange = new Exchange<THandler, TMessage, TResult>(pipeline, _logger);

        _logger.LogDebug(
            "Built exchange. {HandlerType} {MessageType} {ResultType}",
            TypeCache<THandler>.FullName,
            TypeCache<TMessage>.FullName,
            TypeCache<TResult>.FullName
        );

        return exchange;
    }

    private AsyncPipeline<IExchangeContext<TMessage>, TResult> BuildPipeline()
    {
        var pipelineBuilder = new ExchangePipelineBuilder<TMessage, TResult>();

        foreach (var specification in _specifications)
        {
            specification.Apply(pipelineBuilder);
        }

        pipelineBuilder.UsePipe(MessageHandlerInvoker.Invoke<THandler, TMessage, TResult>);

        return pipelineBuilder.Build();
    }
}