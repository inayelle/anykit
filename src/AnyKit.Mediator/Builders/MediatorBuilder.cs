using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace AnyKit.Mediator;

/// <summary>
/// Facilitates the configuration and building of a mediator by adding message handlers and exchange specifications.
/// </summary>
public sealed class MediatorBuilder<THub> : IExchangeBuilder
    where THub : class, IExchangeHub
{
    private readonly IServiceCollection _services;

    internal MediatorBuilder(IServiceCollection services)
    {
        if (services.Any(descriptor => descriptor.ServiceType == typeof(Mediator)))
        {
            throw new DuplicateConfigurationException();
        }
        
        _services = services;

        _services
            .AddSingleton(MediatorLoggerFactory.Create)
            .AddSingleton<Mediator>()
            .AddSingleton<IMediator>(provider => provider.GetRequiredService<Mediator>())
            .AddSingleton<ISender>(provider => provider.GetRequiredService<Mediator>())
            .AddSingleton<IPublisher>(provider => provider.GetRequiredService<Mediator>())
            .AddSingleton<IExchangeHub, THub>();
    }

    /// <summary>
    /// Adds message handlers and exchanges for them from the specified assemblies.
    /// </summary>
    /// <param name="assemblies">The assemblies containing message handlers to be added.</param>
    /// <remarks>
    /// Includes internal types. Does not include abstract and generic types.
    /// </remarks>
    public void AddMessageHandlers(params Assembly[] assemblies)
    {
        var typeInfos = assemblies
            .SelectMany(assembly => assembly.GetTypes())
            .Where(type => type is { IsClass: true, IsAbstract: false, IsGenericType: false })
            .Select(type => type.GetTypeInfo());

        foreach (var typeInfo in typeInfos)
        {
            _services.TryAddExchanges(typeInfo);
            _services.TryAddConfigurators(typeInfo);
        }
    }

    /// <summary>
    /// Adds a shared exchange specification that will be used for all exchanges.
    /// </summary>
    /// <param name="specification">The exchange specification to apply.</param>
    public void AddExchangeSpecification(IExchangeSpecification specification)
    {
        ArgumentNullException.ThrowIfNull(specification);

        _services.AddSingleton(specification);
    }
}