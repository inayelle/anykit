using AnyKit.Pipelines;

namespace AnyKit.Mediator;

/// <inheritdoc cref="IExchangePipelineBuilder{TMessage,TResult}"/>
internal sealed class ExchangePipelineBuilder<TMessage, TResult>
    : AsyncPipelineBuilder<IExchangeContext<TMessage>, TResult>, IExchangePipelineBuilder<TMessage, TResult>
{
    /// <inheritdoc cref="IExchangePipelineBuilder{TMessage,TResult}"/>
    public void UsePipe(IExchangePipe pipe)
    {
        base.UsePipe(pipe.Invoke);
    }

    /// <inheritdoc cref="IExchangePipelineBuilder{TMessage,TResult}"/>
    public void UseGenericPipe(IExchangePipe<TMessage, TResult> pipe)
    {
        base.UsePipe(pipe.Invoke);
    }

    /// <inheritdoc cref="IExchangePipelineBuilder{TMessage,TResult}"/>
    public void UsePipe<TPipe>()
        where TPipe : class, IExchangePipe
    {
        base.UsePipe(ActivatorPipeInvoker.Invoke<TPipe, TMessage, TResult>);
    }

    /// <inheritdoc cref="IExchangePipelineBuilder{TMessage,TResult}"/>
    public void UseGenericPipe<TPipe>()
        where TPipe : class, IExchangePipe<TMessage, TResult>
    {
        base.UsePipe(ActivatorPipeInvoker.InvokeGeneric<TPipe, TMessage, TResult>);
    }
}