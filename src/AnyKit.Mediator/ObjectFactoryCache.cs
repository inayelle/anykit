using Microsoft.Extensions.DependencyInjection;

namespace AnyKit.Mediator;

internal static class ObjectFactoryCache<T>
    where T : class
{
    public static ObjectFactory<T> Value { get; } = ActivatorUtilities.CreateFactory<T>([]);
}