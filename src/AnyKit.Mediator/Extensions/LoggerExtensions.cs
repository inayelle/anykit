using Microsoft.Extensions.Logging;

namespace AnyKit.Mediator;

internal static class MediatorLoggerExtensions
{
    public static void LogExchangeStarted<THandler, TMessage, TResult>(
        this IMediatorLogger logger,
        CorrelationId correlationId
    ) where THandler : class, IMessageHandler<TMessage, TResult>
    {
        logger.Log(
            LogLevel.Debug,
            "Exchange execution started. {CorrelationId} {HandlerType} {MessageType} {ResultType}",
            correlationId,
            TypeCache<THandler>.FullName,
            TypeCache<TMessage>.FullName,
            TypeCache<TResult>.FullName
        );
    }

    public static void LogExchangeCompleted<THandler, TMessage, TResult>(
        this IMediatorLogger logger,
        CorrelationId correlationId,
        TimeSpan elapsed
    ) where THandler : class, IMessageHandler<TMessage, TResult>
    {
        logger.Log(
            LogLevel.Debug,
            "Exchange execution completed. {CorrelationId} {HandlerType} {MessageType} {ResultType} {Elapsed}",
            correlationId,
            TypeCache<THandler>.FullName,
            TypeCache<TMessage>.FullName,
            TypeCache<TResult>.FullName,
            elapsed
        );
    }

    public static void LogExchangeFaulted<THandler, TMessage, TResult>(
        this IMediatorLogger logger,
        CorrelationId correlationId,
        TimeSpan elapsed,
        Exception exception
    ) where THandler : class, IMessageHandler<TMessage, TResult>
    {
        logger.Log(
            LogLevel.Error,
            exception,
            "Exchange execution faulted. {CorrelationId} {HandlerType} {MessageType} {ResultType} {Elapsed}",
            correlationId,
            TypeCache<THandler>.FullName,
            TypeCache<TMessage>.FullName,
            TypeCache<TResult>.FullName,
            elapsed
        );
    }
}