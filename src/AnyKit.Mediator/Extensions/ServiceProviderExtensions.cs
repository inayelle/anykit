using Microsoft.Extensions.DependencyInjection;

namespace AnyKit.Mediator;

internal static class ServiceProviderExtensions
{
    public static T CreateInstance<T>(this IServiceProvider serviceProvider)
        where T : class
    {
        return ActivatorUtilities.CreateInstance<T>(serviceProvider);
    }

    public static T CreateInstanceWithFactoryCache<T>(this IServiceProvider serviceProvider)
        where T : class
    {
        return ObjectFactoryCache<T>.Value.Invoke(serviceProvider, []);
    }

    public static InstanceScope<T> CreateInstanceScope<T>(this IServiceProvider serviceProvider)
        where T : class
    {
        var instance = ActivatorUtilities.CreateInstance<T>(serviceProvider);

        return new InstanceScope<T>(instance);
    }

    public static InstanceScope<T> CreateInstanceScopeWithFactoryCache<T>(this IServiceProvider serviceProvider)
        where T : class
    {
        var instance = ObjectFactoryCache<T>.Value.Invoke(serviceProvider, []);

        return new InstanceScope<T>(instance);
    }
}