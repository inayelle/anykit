using System.Diagnostics.CodeAnalysis;
using System.Reflection;

namespace AnyKit.Mediator;

internal static class TypeExtensions
{
    public static IEnumerable<IExchangeDescriptor> GetExchangeDescriptors(this TypeInfo typeInfo)
    {
        foreach (var @interface in typeInfo.ImplementedInterfaces)
        {
            if (!TryGetMessageHandlerArguments(@interface, out var messageType, out var resultType))
            {
                continue;
            }

            var descriptorType = typeof(ExchangeDescriptor<,,>).MakeGenericType(
                typeInfo,
                messageType,
                resultType
            );

            yield return (IExchangeDescriptor)Activator.CreateInstance(descriptorType);
        }
    }

    public static IEnumerable<Type> GetConfiguratorTypes(this TypeInfo typeInfo)
    {
        return typeInfo
           .ImplementedInterfaces
           .Where(@interface => @interface.IsConstructedGenericType)
           .Where(@interface => @interface.GetGenericTypeDefinition() == typeof(IExchangeConfigurator<,,>));
    }

    private static bool TryGetMessageHandlerArguments(
        Type interfaceType,
        [NotNullWhen(true)] out Type messageType,
        [NotNullWhen(true)] out Type resultType
    )
    {
        if (interfaceType is not { IsConstructedGenericType: true })
        {
            messageType = null;
            resultType = null;
            return false;
        }

        var interfaceDefinition = interfaceType.GetGenericTypeDefinition();

        if (interfaceDefinition == typeof(IMessageHandler<,>))
        {
            var genericArguments = interfaceType.GetGenericArguments();

            messageType = genericArguments[0];
            resultType = genericArguments[1];

            return true;
        }

        messageType = null;
        resultType = null;
        return false;
    }
}