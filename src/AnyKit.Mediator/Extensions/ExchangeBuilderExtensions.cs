namespace AnyKit.Mediator;

/// <summary>
/// Provides extension methods for configuring exchange pipelines using an <see cref="IExchangeBuilder"/>.
/// </summary>
public static class ExchangeBuilderExtensions
{
    /// <summary>
    /// Adds a specified pipe instance to the exchange pipeline.
    /// </summary>
    /// <param name="builder">The exchange builder used to configure the pipeline.</param>
    /// <param name="pipe">The pipe instance to add to the pipeline.</param>
    public static void UsePipe(this IExchangeBuilder builder, IExchangePipe pipe)
    {
        ArgumentNullException.ThrowIfNull(builder);
        ArgumentNullException.ThrowIfNull(pipe);

        builder.AddExchangeSpecification(new InstancePipeSpecification(pipe));
    }

    /// <summary>
    /// Adds a pipe to the exchange pipeline. The pipe will be activated through <see cref="IServiceProvider"/> from the current exchange context.
    /// </summary>
    /// <typeparam name="TPipe">The type of the pipe to add to the pipeline.</typeparam>
    /// <param name="builder">The exchange builder used to configure the pipeline.</param>
    public static void UsePipe<TPipe>(this IExchangeBuilder builder)
        where TPipe : class, IExchangePipe
    {
        ArgumentNullException.ThrowIfNull(builder);

        builder.AddExchangeSpecification(new ServicePipeSpecification<TPipe>());
    }
}