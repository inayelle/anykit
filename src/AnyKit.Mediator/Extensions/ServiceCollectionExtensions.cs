using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace AnyKit.Mediator;

/// <summary>
/// Provides extension methods for adding and configuring mediator to <see cref="IServiceCollection"/>.
/// </summary>
public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Registers and configures the mediator and underlying services with the specified configuration action.
    /// </summary>
    /// <param name="services">The service collection to which the mediator will be added.</param>
    /// <param name="configure">An action to configure the <see cref="MediatorBuilder{THub}"/> instance.</param>
    /// <exception cref="DuplicateConfigurationException">Thrown when method <see cref="AddMediator"/> is called more than once.</exception>
    public static void AddMediator<THub>(this IServiceCollection services, Action<MediatorBuilder<THub>> configure)
        where THub : class, IExchangeHub
    {
        ArgumentNullException.ThrowIfNull(services);
        ArgumentNullException.ThrowIfNull(configure);

        var builder = new MediatorBuilder<THub>(services);

        configure.Invoke(builder);
    }

    public static void AddMediator(
        this IServiceCollection services,
        Action<MediatorBuilder<DefaultExchangeHub>> configure
    )
    {
        services.AddMediator<DefaultExchangeHub>(configure);
    }

    internal static void TryAddExchanges(this IServiceCollection services, TypeInfo typeInfo)
    {
        var exchangeDescriptors = typeInfo.GetExchangeDescriptors();

        foreach (var exchangeDescriptor in exchangeDescriptors)
        {
            var serviceDescriptor = exchangeDescriptor.MakeServiceDescriptor(ServiceLifetime.Singleton);

            services.Add(serviceDescriptor);
        }
    }

    internal static void TryAddConfigurators(this IServiceCollection services, TypeInfo typeInfo)
    {
        var configuratorTypes = typeInfo.GetConfiguratorTypes();

        foreach (var configuratorType in configuratorTypes)
        {
            services.AddSingleton(configuratorType, typeInfo);
        }
    }
}