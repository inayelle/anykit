using System.Diagnostics;
using AnyKit.Pipelines;

namespace AnyKit.Mediator;

/// <inheritdoc/>
internal sealed class Exchange<THandler, TMessage, TResult> : IExchange<TMessage, TResult>
    where THandler : class, IMessageHandler<TMessage, TResult>
{
    private readonly AsyncPipeline<IExchangeContext<TMessage>, TResult> _pipeline;
    private readonly IMediatorLogger _logger;

    public Exchange(
        AsyncPipeline<IExchangeContext<TMessage>, TResult> pipeline,
        IMediatorLogger logger
    )
    {
        _pipeline = pipeline;
        _logger = logger;
    }

    /// <inheritdoc/>
    public async Task<TResult> Execute(IExchangeContext<TMessage> context)
    {
        _logger.LogExchangeStarted<THandler, TMessage, TResult>(context.CorrelationId);

        var stopwatch = Stopwatch.GetTimestamp();

        try
        {
            var result = await _pipeline.Invoke(context).ConfigureAwait(false);

            _logger.LogExchangeCompleted<THandler, TMessage, TResult>(
                context.CorrelationId,
                Stopwatch.GetElapsedTime(stopwatch)
            );

            return result;
        }
        catch (Exception exception)
        {
            _logger.LogExchangeFaulted<THandler, TMessage, TResult>(
                context.CorrelationId,
                Stopwatch.GetElapsedTime(stopwatch),
                exception
            );

            throw;
        }
    }
}