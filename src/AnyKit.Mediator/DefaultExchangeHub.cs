namespace AnyKit.Mediator;

/// <inheritdoc/>
public sealed class DefaultExchangeHub : ExchangeHub
{
    public DefaultExchangeHub(IServiceProvider serviceProvider) : base(serviceProvider)
    {
    }
}