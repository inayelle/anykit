using Microsoft.Extensions.DependencyInjection;

namespace AnyKit.Mediator;

/// <inheritdoc/>
public abstract class ExchangeHub : IExchangeHub
{
    private readonly IServiceProvider _serviceProvider;

    protected ExchangeHub(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    /// <inheritdoc/>
    public Task<TResult> Send<TMessage, TResult>(MessageContext<TMessage> context)
    {
        if (context.CancellationToken.IsCancellationRequested)
        {
            return Task.FromCanceled<TResult>(context.CancellationToken);
        }

        var exchanges = GetExchanges<TMessage, TResult>();

        var exchange = exchanges.Length switch
        {
            1 => exchanges[0],
            0 => throw new MissingExchangeException(typeof(TMessage), typeof(TResult)),
            _ => throw new AmbiguousExchangesException(typeof(TMessage), typeof(TResult)),
        };

        var state = CreateState(context, exchangesCount: 1);

        return ExecuteExchange(exchange, context, state);
    }

    /// <inheritdoc/>
    public Task Publish<TMessage>(MessageContext<TMessage> context)
    {
        if (context.CancellationToken.IsCancellationRequested)
        {
            return Task.FromCanceled(context.CancellationToken);
        }

        var exchanges = GetExchanges<TMessage, None>();

        if (exchanges.Length == 0)
        {
            return Task.CompletedTask;
        }

        var state = CreateState(context, exchanges.Length);

        foreach (var exchange in exchanges.AsSpan())
        {
            Task.Run(() => ExecuteExchange(exchange, context, state));
        }

        return Task.CompletedTask;
    }

    protected virtual IDisposable CreateState<TMessage>(MessageContext<TMessage> context, int exchangesCount)
    {
        return State.Instance;
    }

    private async Task<TResult> ExecuteExchange<TMessage, TResult>(
        IExchange<TMessage, TResult> exchange,
        MessageContext<TMessage> context,
        IDisposable state
    )
    {
        using var _ = state;

        var serviceScope = _serviceProvider.CreateAsyncScope();
        await using var __ = serviceScope.ConfigureAwait(false);

        var exchangeContext = new ExchangeContext<TMessage>(
            this,
            context.Message,
            serviceScope.ServiceProvider,
            context.CorrelationId,
            context.CancellationToken
        );

        return await exchange.Execute(exchangeContext).ConfigureAwait(false);
    }

    private IExchange<TMessage, TResult>[] GetExchanges<TMessage, TResult>()
    {
        var exchanges = _serviceProvider.GetServices<IExchange<TMessage, TResult>>();

        return exchanges switch
        {
            IExchange<TMessage, TResult>[] array => array,
            _ => exchanges.ToArray(),
        };
    }
    
    private sealed class State : IDisposable
    {
        public static State Instance { get; } = new();

        private State()
        {
        }

        public void Dispose()
        {
        }
    }
}