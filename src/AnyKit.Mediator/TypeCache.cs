using System.Collections.Frozen;

namespace AnyKit.Mediator;

internal static class TypeCache<T>
{
    public static string FullName { get; } = CreateDisplayName(typeof(T));

    private static string CreateDisplayName(Type type)
    {
        switch (type)
        {
            case { IsGenericType: true } when type.GetGenericTypeDefinition() == typeof(Nullable<>):
            {
                var underlyingType = Nullable.GetUnderlyingType(type);

                return CreateDisplayName(underlyingType) + '?';
            }
            case { IsGenericType: true }:
            {
                var joined = String.Join(", ", type.GenericTypeArguments.Select(CreateDisplayName));

                return $"{type.Name.Split('`')[0]}<{joined}>";
            }
            case { IsArray: true }:
            {
                var elementType = type.GetElementType();

                return CreateDisplayName(elementType) + "[]";
            }
            default:
            {
                return KeywordTypes.GetOrDefault(type);
            }
        }
    }
}

file static class KeywordTypes
{
    private static readonly FrozenDictionary<Type, string> Mapping;

    static KeywordTypes()
    {
        var mapping = new Dictionary<Type, string>
        {
            [typeof(Boolean)] = "bool",
            [typeof(Byte)] = "byte",
            [typeof(Char)] = "char",
            [typeof(Decimal)] = "decimal",
            [typeof(Double)] = "double",
            [typeof(Single)] = "float",
            [typeof(Int32)] = "int",
            [typeof(Int64)] = "long",
            [typeof(SByte)] = "sbyte",
            [typeof(Int16)] = "short",
            [typeof(String)] = "string",
            [typeof(UInt32)] = "uint",
            [typeof(UInt64)] = "ulong",
            [typeof(UInt16)] = "ushort",
        };

        Mapping = mapping.ToFrozenDictionary();
    }

    public static string GetOrDefault(Type type)
    {
        return Mapping.TryGetValue(type, out var keyword)
            ? keyword
            : type.FullName;
    }
}