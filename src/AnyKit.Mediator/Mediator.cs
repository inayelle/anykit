namespace AnyKit.Mediator;

/// <inheritdoc/>
internal sealed class Mediator : IMediator
{
    private readonly IExchangeHub _exchangeHub;

    public Mediator(IExchangeHub exchangeHub)
    {
        _exchangeHub = exchangeHub;
    }

    /// <inheritdoc/>
    public Task<TResult> Send<TMessage, TResult>(
        TMessage message,
        CancellationToken cancellationToken = default
    )
    {
        var context = new MessageContext<TMessage>(message, CorrelationId.Create(), cancellationToken);

        return _exchangeHub.Send<TMessage, TResult>(context);
    }

    /// <inheritdoc/>
    public Task Send<TMessage>(TMessage message, CancellationToken cancellationToken = default)
    {
        var context = new MessageContext<TMessage>(message, CorrelationId.Create(), cancellationToken);

        return _exchangeHub.Send<TMessage, None>(context);
    }

    /// <inheritdoc/>
    public Task Publish<TMessage>(
        TMessage message,
        CancellationToken cancellationToken = default
    )
    {
        var context = new MessageContext<TMessage>(message, CorrelationId.Create(), cancellationToken);

        return _exchangeHub.Publish(context);
    }
}