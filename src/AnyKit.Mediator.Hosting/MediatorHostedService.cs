using Microsoft.Extensions.Hosting;

namespace AnyKit.Mediator;

internal sealed class MediatorHostedService : IHostedLifecycleService
{
    private readonly IExchangeHub _exchangeHub;

    public MediatorHostedService(IExchangeHub exchangeHub)
    {
        _exchangeHub = exchangeHub;
    }

    public async Task StoppingAsync(CancellationToken cancellationToken)
    {
        if (_exchangeHub is not HostedExchangeHub hostedExchangeHub)
        {
            return;
        }

        await hostedExchangeHub
            .LockAndWait()
            .WaitAsync(cancellationToken)
            .ConfigureAwait(false);
    }
    
    public Task StartingAsync(CancellationToken cancellationToken) => Task.CompletedTask;
    
    public Task StartAsync(CancellationToken cancellationToken) => Task.CompletedTask;

    public Task StartedAsync(CancellationToken cancellationToken) => Task.CompletedTask;

    public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;

    public Task StoppedAsync(CancellationToken cancellationToken) => Task.CompletedTask;
}