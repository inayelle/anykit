namespace AnyKit.Mediator;

public sealed class HostedExchangeHub : ExchangeHub
{
    private readonly ExchangeLeaseTracker _exchangeLeaseTracker;

    public HostedExchangeHub(IServiceProvider serviceProvider) : base(serviceProvider)
    {
        _exchangeLeaseTracker = new ExchangeLeaseTracker();
    }

    protected override IDisposable CreateState<TMessage>(MessageContext<TMessage> context, int exchangesCount)
    {
        return _exchangeLeaseTracker.Acquire(context.CorrelationId, exchangesCount);
    }

    internal Task LockAndWait()
    {
        return _exchangeLeaseTracker.LockAndWait();
    }
}