using Microsoft.Extensions.DependencyInjection;

namespace AnyKit.Mediator;

public static class ServiceCollectionExtensions
{
    public static void AddHostedMediator(
        this IServiceCollection services,
        Action<MediatorBuilder<HostedExchangeHub>> configure
    )
    {
        services.AddMediator(configure);

        services.AddHostedService<MediatorHostedService>();
    }
}