using System.Collections.Concurrent;

namespace AnyKit.Mediator;

internal sealed class ExchangeLeaseTracker
{
    private const int Unlocked = 0;
    private const int Locked = 1;

    private readonly ConcurrentDictionary<CorrelationId, Lease> _leases;
    private readonly TaskCompletionSource _completionSource;

    private int _state;

    public ExchangeLeaseTracker()
    {
        _leases = new ConcurrentDictionary<CorrelationId, Lease>();
        _completionSource = new TaskCompletionSource(TaskCreationOptions.RunContinuationsAsynchronously);

        _state = Unlocked;
    }

    public IDisposable Acquire(CorrelationId correlationId, int count)
    {
        return _leases.AddOrUpdate(correlationId, Create, Share);

        Lease Create(CorrelationId id)
        {
            if (_state == Locked)
            {
                throw new HubClosedException();
            }

            return new Lease(this, id, count);
        }

        Lease Share(CorrelationId _, Lease lease)
        {
            lease.Share(count);

            return lease;
        }
    }

    public Task LockAndWait()
    {
        Interlocked.Exchange(ref _state, Locked);

        return _leases.IsEmpty
            ? Task.CompletedTask
            : _completionSource.Task;
    }

    private void Release(CorrelationId correlationId)
    {
        _leases.TryRemove(correlationId, out _);

        if (_state == Locked && _leases.IsEmpty)
        {
            _completionSource.TrySetResult();
        }
    }

    private sealed class Lease : IDisposable
    {
        private readonly ExchangeLeaseTracker _leaseTracker;
        private readonly CorrelationId _correlationId;

        private int _counter;

        public Lease(ExchangeLeaseTracker leaseTracker, CorrelationId correlationId, int count)
        {
            _correlationId = correlationId;
            _leaseTracker = leaseTracker;

            _counter = count;
        }

        public void Share(int count)
        {
            Interlocked.Add(ref _counter, count);
        }

        public void Dispose()
        {
            var counter = Interlocked.Decrement(ref _counter);

            if (counter == 0)
            {
                _leaseTracker.Release(_correlationId);
            }
        }
    }
}