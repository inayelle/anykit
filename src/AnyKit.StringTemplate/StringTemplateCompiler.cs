using System.Text;

namespace AnyKit.StringTemplate;

internal static class StringTemplateCompiler
{
    public static string Compile(
        string template,
        IReadOnlyDictionary<int, StringTemplatePlaceholder> placeholders,
        IReadOnlyDictionary<string, object> values
    )
    {
        if (placeholders.Count == 0)
        {
            return template;
        }

        var builder = new StringBuilder(template.Length);

        var index = 0;
        while (index < template.Length)
        {
            if (placeholders.TryGetValue(index, out var placeholder) &&
                values.TryGetValue(placeholder.Key, out var value))
            {
                builder.Append(value);
                index += placeholder.Length;
            }
            else
            {
                builder.Append(template[index]);
                index += 1;
            }
        }

        return builder.ToString();
    }
}