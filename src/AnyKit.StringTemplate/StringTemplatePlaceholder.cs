namespace AnyKit.StringTemplate;

internal readonly struct StringTemplatePlaceholder
{
    public required string Key { get; init; }
    public required int Length { get; init; }
}