namespace AnyKit.StringTemplate;

public sealed class StringTemplateScope
{
    private readonly string _template;
    private readonly IReadOnlyDictionary<int, StringTemplatePlaceholder> _placeholders;
    private readonly Dictionary<string, object> _values;

    internal StringTemplateScope(
        string template,
        IReadOnlyDictionary<int, StringTemplatePlaceholder> placeholders
    )
    {
        _template = template;
        _placeholders = placeholders;
        _values = new Dictionary<string, object>(_placeholders.Count);
    }

    public StringTemplateScope Add(string key, object value)
    {
        _values[key] = value;

        return this;
    }

    public string Compile()
    {
        return StringTemplateCompiler.Compile(_template, _placeholders, _values);
    }
}