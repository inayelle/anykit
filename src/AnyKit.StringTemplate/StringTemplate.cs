namespace AnyKit.StringTemplate;

public class StringTemplate
{
    private const string PlaceholderStart = "{{";
    private const string PlaceholderEnd = "}}";

    private readonly string _template;
    private readonly IReadOnlyDictionary<int, StringTemplatePlaceholder> _placeholders;

    public StringTemplate(string template)
    {
        _template = template;
        _placeholders = ParsePlaceholders(template);
    }

    public string Compile(IReadOnlyDictionary<string, object> values)
    {
        return StringTemplateCompiler.Compile(_template, _placeholders, values);
    }

    public StringTemplateScope CreateScope()
    {
        return new StringTemplateScope(_template, _placeholders);
    }

    private static Dictionary<int, StringTemplatePlaceholder> ParsePlaceholders(string template)
    {
        var placeholders = new Dictionary<int, StringTemplatePlaceholder>();

        var span = template.AsSpan();
        var charIndex = 0;

        while (charIndex < span.Length)
        {
            if (!TryParsePlaceholder(span[charIndex..], out var key, out var length))
            {
                ++charIndex;
                continue;
            }

            var placeholder = new StringTemplatePlaceholder
            {
                Key = key,
                Length = length
            };

            placeholders.Add(charIndex, placeholder);
            charIndex += length;
        }

        return placeholders;
    }

    private static bool TryParsePlaceholder(
        ReadOnlySpan<char> span,
        out string key,
        out int length
    )
    {
        if (!span.StartsWith(PlaceholderStart))
        {
            key = null;
            length = default;
            return false;
        }

        if (span.Length > 2 && span[2] == '{')
        {
            key = null;
            length = default;
            return false;
        }

        length = PlaceholderStart.Length;
        var found = false;
        while (length < span.Length)
        {
            if (span[length..].StartsWith(PlaceholderEnd))
            {
                length += PlaceholderEnd.Length;
                found = true;
                break;
            }

            ++length;
        }

        var keyLength = length - PlaceholderStart.Length - PlaceholderEnd.Length;

        if (!found || keyLength == 0)
        {
            key = null;
            length = default;
            return false;
        }

        key = span.Slice(PlaceholderStart.Length, keyLength).ToString();
        return true;
    }
}