# AnyKit.Mediator 8.1.7

## Features
- Implement graceful shutdown by integrating with Microsoft.Extensions.Hosting

## Fixes
- Fix handling published events twice for events with a single consumer